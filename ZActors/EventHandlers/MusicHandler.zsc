class MusicHandler : EventHandler
{
	const BEMapMusicCount = 334;
	const GameOverMusicCount = 4;

	const LevelMusicPrefix = "BELMS";
	const GameOverMusicPrefix = "GAMEOVR";

	string CurrentMusic;
	string PreviousMusic;

	int MusicInfoTicker; // [Ace] This is the timer that determines when to show the info on the HUD. Alpha interpolation is done separately in the HUD code.
	const MusicInfoDuration = 34 * 10;

	private bool IsFiltered(string trackName) // [Ace] The track name as defined in LANGUAGE.
	{
		string LowerTrackName = trackName.MakeLower();

		Array<string> FilteredNames;
		string RawFilterText = bulleteye_musicfilter;
		string LowerFilterText;
		if (RawFilterText != "")
		{
			LowerFilterText = RawFilterText.MakeLower();
			LowerFilterText.Split(FilteredNames, ",");
		}

		// String.Split won't even bother making an array if there's only 1 string after the split.
		if (FilteredNames.Size() > 0)
		{
			for (int i = 0; i < FilteredNames.Size(); ++i)
			{
				if (LowerTrackName.IndexOf(FilteredNames[i]) > -1)
				{
					return true;
				}
			}
		}
		
		return LowerTrackName.IndexOf(LowerFilterText) > -1;
	}

	private void PlayGameOverMusic()
	{
		int MusNumber = random(1, GameOverMusicCount);
		PreviousMusic = CurrentMusic;
		CurrentMusic = GameOverMusicPrefix..MusNumber;
		S_ChangeMusic(CurrentMusic);
		MusicInfoTicker = 0;
	}

	private void PlayRandomMapMusic(bool usePrevious)
	{
		// [Ace] If custom map music is disabled, use the default music for the map.
		if (GameState == GS_TITLELEVEL || bulleteye_musicmode == 0)
		{
			S_ChangeMusic("*");
			return;
		}

		// [Ace] Otherwise, play a random track or previous one, depending on 'usePrevious'.
		// Apply blacklist/whitelist if possible. 
		// The CheckedNumbers array works the same way as the bonus array in BEEquipmentItemBase.zsc
		string LocalizedString;
		Array<int> CheckedNumbers;
		do
		{
			int MusNumber = random(1, BEMapMusicCount);
			if (CheckedNumbers.Size() > 0 && CheckedNumbers.Find(MusNumber) != CheckedNumbers.Size()) continue;

			CheckedNumbers.Push(MusNumber);

			CurrentMusic = usePrevious && PreviousMusic != "" ? PreviousMusic : LevelMusicPrefix..MusNumber;
			LocalizedString = StringTable.Localize("$"..CurrentMusic);
			
			if (bulleteye_filtermode == 0 || usePrevious || (bulleteye_filtermode == 1 && IsFiltered(LocalizedString) || bulleteye_filtermode == 2 && !IsFiltered(LocalizedString))) break;
		}
		while (CheckedNumbers.Size() < BEMapMusicCount)
		S_ChangeMusic(CurrentMusic);
		MusicInfoTicker = 0;
	}

	override void WorldTick()
	{
		if (MusicInfoTicker > -1)
		{
			MusicInfoTicker++;
		}

		if (MusicInfoTicker >= MusicInfoDuration)
		{
			MusicInfoTicker = -1;
		}
	}

	override void WorldLoaded(WorldEvent e)
	{
		PlayRandomMapMusic(false);
	}

	// [Ace] This is for the Game Over music. Only works in single player.
	override void PlayerDied(PlayerEvent e)
	{
		if (multiplayer || GameState == GS_TITLELEVEL || bulleteye_musicmode == 0)
		{
			return;
		}

		PlayGameOverMusic();
	}

	// [Ace] In arcade mode you can respawn. This resumes normal music. Only works in single player.
	override void PlayerRespawned(PlayerEvent e)
	{
		if (multiplayer || GameState == GS_TITLELEVEL || bulleteye_musicmode == 0)
		{
			return;
		}
		
		PlayRandomMapMusic(true);
	}
	
	override void NetworkProcess(ConsoleEvent e)
	{
		// [Ace] Re-roll.
		if (e.Name == 'BulletEyeMapMusReRoll')
		{
			PlayRandomMapMusic(false);
		}

		// [Ace] Show info.
		if (e.Name == 'ShowTrackInfo')
		{
			MusicInfoTicker = 0;
		}
	}
}