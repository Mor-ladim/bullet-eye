class VampiricGauntletsWeapon : BEWeaponBase
{
	override void DoEffect()
	{
		MassHealTimer--;

		Super.DoEffect();
	}

	action void A_AddCharge(int amount)
	{
		invoker.GauntletsCharge = min(invoker.GauntletsCharge + amount, MaxGauntletsCharge);
	}

	// [Ace] It reduces the charge and returns the amount it was decreased by.
	// I can't just return amount because it would then heal for 'amount', even if the charge is lower than that number.
	action int A_TakeCharge(int amount)
	{
		int OriginalCharge = invoker.GauntletsCharge;
		invoker.GauntletsCharge = max(invoker.GauntletsCharge - amount, 0);

		return OriginalCharge - invoker.GauntletsCharge;
	}

	// [Ace] There is a bug here. Excess used charges could sometimes be discarded when reaching the heal limit.
	// Fixable, but it's not worth it. Too much pain, not enough profit.
	action void A_HealTargetOrSelf()
	{
		int MaxHealAmount = invoker.WeaponLevel >= 2 ? 80 : 60;

		// [Ace] Heal target.
		Actor a = AimTarget();
		if (a && a is "PlayerPawn" && Distance3D(a) <= (invoker.WeaponLevel >= 2 ? 256 : 128) && a.Health < MaxHealAmount)
		{
			int HealAmount = A_TakeCharge(5);
			if (HealAmount > 0)
			{
				a.GiveBody(HealAmount, MaxHealAmount);
				A_PlaySound("weapons/vampiricgauntletsheal", CHAN_AUTO, 2, 0);
			}
		}

		// [Ace] Heal self.
		else if (Health < MaxHealAmount)
		{
			int HealAmount = A_TakeCharge(5);
			if (HealAmount > 0)
			{
				GiveBody(HealAmount, MaxHealAmount);
				A_PlaySound("weapons/vampiricgauntletsheal", CHAN_AUTO, 2, 0);
			}
		}
	}

	action void A_MassHeal()
	{
		int MaxHealAmount = invoker.WeaponLevel >= 2 ? 80 : 60;
		int HealAmount = A_TakeCharge(100);

		let Iterator = BlockThingsIterator.Create(self, 512);
		while (Iterator.Next())
		{
			if (Iterator.thing is "PlayerPawn")
			{
				Iterator.thing.GiveBody(HealAmount, MaxHealAmount);
			}
		}

		invoker.MassHealTimer = invoker.MassHealCooldown;
	}

	int GauntletsCharge;
	const MaxGauntletsCharge = 150;

	int MassHealTimer;
	const MassHealCooldown = 35 * 10;

	Default
	{
		Inventory.PickupSound "misc/w_pkup";
		Inventory.PickupMessage "";
		Inventory.Icon "WEAP19";
		Weapon.SlotNumber 1;
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.AmmoType1 "AmmoVampiricGauntlets";
		Weapon.AmmoType2 "AmmoSupportEnergy";
		Weapon.UpSound "beplayer/weapswitchvampiricgauntlets";
		BEWeaponBase.WeaponPowerupItem "NewVampiricGauntletsPowerup";
		BEWeaponBase.ReloadProperties "weapons/vampiricgauntletsreloadstage1", "weapons/vampiricgauntletsreloadstage2", "weapons/vampiricgauntletsreloadstage3";
		BEWeaponBase.Sprite "WEAPH19";
		BEWeaponBase.Type WTYPE_Support;
		BEWeaponBase.Attributes 3, 5, 3, 3;
		BEWeaponBase.Durability 800;
		BEWeaponBase.EffectTexts "- Primary fire heals yourself or teammates. Alt-fire drains health from enemies and stores healing energy. Up to a max of 60 health can be restored.", "- Increases range and up to 80 health can be restored.", "- When at a high charge level, looking straight up and using alt-fire triggers a mass heal. Can only be used once every 10 seconds.";
		BEWeaponBase.FlavorText "Technologically advanced gloves with a medieval look - as one would expect from the Einhander corporation. These gauntlets can drain the very life essence from foes and convert it to a special energy that can heal wounds. Though, it is not exactly known how the technology functions, one thing is for sure - as long as there are living enemies to drain life energy from, the potential of unlimited power is in your grasp.";
		+WEAPON.ALT_AMMO_OPTIONAL
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.CHEATNOTWEAPON
		Tag "Vampiric Gauntlets";
	}
 
	States
	{
		Spawn:
			_WVG A -1;
			Loop;
		Ready:
			_WVG A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Loop;
			_WVG A 1 Offset(0, 80); //Head here after Sub-Weapon usage.
			_WVG A 1 Offset(0, 60);
			_WVG A 1 Offset(0, 43);
			TNT1 A 0 A_WeaponReady(WRF_NOFIRE|WRF_NOSWITCH);
			Goto Ready;
		Deselect:
			TNT1 AA 0 A_Lower;
			_WVG A 1 A_Lower;
			Loop;
		Select:
			_WVG A 0 A_StopSound(5);
			_WVG A 0 A_SelectWeapon("IronAnnihilator");
		SelectFall:
			TNT1 AA 0 A_Raise;
			_WVG A 1 A_Raise;
			Loop;
		Fire:
			_WVG A 0 A_CheckWeaponFire;
			_WVG A 0 A_JumpIf(invoker.WeaponLevel >= 2,"FireL2");
			_WVG AB 1 Offset(0, 32);
			_WVG C 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletsfire",CHAN_WEAPON,3.5,0);
			_WVG A 0 A_FireBullets(0,0,1,10,"VampiricGauntletsDamagePuff",FBF_EXPLICITANGLE,140);
			_WVG A 0 A_TakeAmmo;
			_WVG A 0 A_TakeDurability(1);
			_WVG A 0 A_CheckWeaponFire;
			_WVG D 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG E 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG F 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 4 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 3;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 6 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;
		FireL2:
			_WVG A 0 A_CheckWeaponFire;
			_WVG AB 1 Offset(0, 32);
			_WVG C 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletsfire",CHAN_WEAPON,3.5,0);
			_WVG A 0 A_FireBullets(0,0,1,10,"VampiricGauntletsDamagePuff",FBF_EXPLICITANGLE,200);
			_WVG A 0 A_TakeAmmo;
			_WVG A 0 A_TakeDurability(1);
			_WVG A 0 A_CheckWeaponFire;
			_WVG D 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG E 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG F 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 4 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 3;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 6 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;			
		Hold:
			_WVG A 0 A_CheckWeaponFire;
			_WVG A 0 A_JumpIf(invoker.WeaponLevel >= 2,"HoldL2");
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletszap",CHAN_AUTO,0.5,0);
			_WVG A 0 A_FireBullets(0,0,1,10,"VampiricGauntletsDamagePuff",FBF_EXPLICITANGLE,140);
			_WVG A 0 A_TakeAmmo;
			_WVG A 0 A_TakeDurability(1);
			_WVG A 0 A_CheckWeaponFire;
			_WVG D 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG E 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG F 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 4 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 3;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 6 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;
		HoldL2:
			_WVG A 0 A_CheckWeaponFire;
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletszap",CHAN_AUTO,0.4,0);
			_WVG A 0 A_FireBullets(0,0,1,10,"VampiricGauntletsDamagePuff",FBF_EXPLICITANGLE,200);
			_WVG A 0 A_TakeAmmo;
			_WVG A 0 A_TakeDurability(1);
			_WVG A 0 A_CheckWeaponFire;
			_WVG D 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG E 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG F 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire("HoldL2");
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 4 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 3;
			_WVG A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 6 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;			
		AltFire:
			_WVG A 0 A_JumpIf(pitch == -90 && invoker.MassHealTimer <= 0 && invoker.GauntletsCharge >= 100 && invoker.WeaponLevel == 3, "MassHeal");
			_WVG AB 1 Offset(0, 32);
			_WVG C 1 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletshealcast",CHAN_WEAPON,3.5,0);
			_WVG A 0 A_HealTargetOrSelf();
			_WVG G 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG H 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG I 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire;
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 1 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 1;
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 6 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;
		AltHold:
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletszap",CHAN_AUTO,0.4,0);
			_WVG A 0 A_HealTargetOrSelf();
			_WVG G 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG H 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG I 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire;
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 4 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 3;
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 6 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;	
		MassHeal:
			_WVG AB 1 Offset(0, 32);
			_WVG C 1 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 0 A_PlaySound("weapons/vampiricgauntletsheal",CHAN_AUTO,3.5,0);
			_WVG A 0 A_MassHeal();
			_WVG G 1 Bright Offset(0, 33);
			_WVG A 0 A_AlertMonsters;
			_WVG A 0 A_GunFlash;
			_WVG H 1 Bright Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG I 1 Bright Offset(0, 33);
			_WVG A 0 A_ReFire;
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG C 1 Offset(0, 32);
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG B 1;
			_WVG A 0 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB);
			_WVG A 18 A_WeaponReady(WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Goto Ready;		
		Flash:
			TNT1 A 1 Bright A_Light1;
			TNT1 A 1 Bright A_Light0;
			Stop;
	}
}

class VampiricGauntletsDamagePuff : BEPuffBase
{
	Default
	{
		+NOINTERACTION
		+ROLLSPRITE
		+BRIGHT
		+PUFFONACTORS
		DamageType "VampiricGauntlets";
		Alpha 0.65;
		Scale 0.6;
		RenderStyle "Add";		
		SeeSound "weapons/vampiricgauntletsdamagehit";
	}

	States
	{
		Spawn:
			VGPF A 1 NoDelay A_SetRoll(roll+random(0,359));
			VGPF A 0 A_SpawnItemEx("VampiricGauntletsPuffGlow",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION);
			VGPF B 1 A_SetRoll(roll+random(0,359));
			VGPF C 1 A_SetRoll(roll+random(0,359));
			VGPF D 1 A_SetRoll(roll+random(0,359));
			Stop;
		Death:    
			VGPF D 1 A_FadeOut(0.1);
			Loop;
	}
}

class VampiricGauntletsPuffGlow : Actor
{
	Default
	{
		+NOINTERACTION
		+BRIGHT
		Alpha 0.85;
		Scale 1.2;
		RenderStyle "Add";
	}

	States
	{
		Spawn:
			VGPE A 2;
			Stop;
		Death:    
			VGPF A 1 A_FadeOut(0.1);
			Loop;
	}
}

class VampiricGauntletsFireFX : Actor
{
	Default
	{
		+NOINTERACTION
		+BRIGHT
		+ROLLSPRITE
		Alpha 0.35;
		Scale 0.8;
		RenderStyle "Add";
	}

	States
	{
		Spawn:
			VGPE B 8 NoDelay A_SetRoll(roll+random(0,359));
			Stop;
		Death:    
			VGPF B 1 A_FadeOut(0.1);
			Loop;
	}
}