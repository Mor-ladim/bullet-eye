//MECHANICS
ACTOR EnergyClawSlash : FastProjectile
{
   Speed 35
   Damage (3) //(2)
   DamageType "SubWeapon"
   PROJECTILE
   +RIPPER
   +BLOODLESSIMPACT
   RenderStyle Add
   Alpha 0.6
   SeeSound "subweapon/energyclawproj"
   ReactionTime 6
   MissileHeight 8
   MissileType "EnergyClawSlashTail"
   States
   {
   Spawn:
      VTSB AB 2 Bright
	  TNT1 A 0 A_CountDown
	  Loop
   Death:
      SPRI A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
      VTSB C 1 Bright
	  TNT1 A 0 A_FadeOut(0.1)
      Loop
   }
}

ACTOR EnergyClawSlashTail
{
	+NOINTERACTION
	+NOCLIP
    +BRIGHT
	Radius 1
	Height 1
	Renderstyle Add
	Alpha 0.3
	Scale 0.8
	States
	{
   Spawn:
      VTSB AB 1 Bright
	  Stop
   Death:
      VTSB C 1 Bright
	  VTSB A 0 A_FadeOut(0.1)
      Loop	  
	}
}

ACTOR EnergyClawSlashRight : FastProjectile
{
   Speed 35
   Damage (3) //(2)
   DamageType "SubWeapon"
   PROJECTILE
   +RIPPER
   +BLOODLESSIMPACT
   RenderStyle Add
   Alpha 0.6
   SeeSound "subweapon/energyclawproj"
   ReactionTime 6
   MissileHeight 8
   MissileType "EnergyClawSlashTailRight"
   States
   {
   Spawn:
      VTSA AB 2 Bright
	  TNT1 A 0 A_CountDown
	  Loop
   Death:
      SPRI A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
      VTSA C 1 Bright
	  TNT1 A 0 A_FadeOut(0.1)
      Loop
   }
}

ACTOR EnergyClawSlashTailRight
{
	+NOINTERACTION
	+NOCLIP
    +BRIGHT
	Radius 1
	Height 1
	Renderstyle Add
	Alpha 0.3
	Scale 0.8
	States
	{
   Spawn:
      VTSA AB 1 Bright
	  Stop
   Death:
      VTSA C 1 Bright
	  VTSA A 0 A_FadeOut(0.1)
      Loop	  
	}
}

ACTOR EnergyClawPuff
{
  +NOBLOCKMAP
  +NOGRAVITY
  +NOEXTREMEDEATH
  +BLOODLESSIMPACT
  +FORCEPAIN
  +PUFFONACTORS
  +ROLLSPRITE
  DamageType "SubWeapon"  
  RenderStyle Add
  Alpha 0.5
  SeeSound "subweapon/energyclawmhit"
  States
  {
  Spawn:
    RMPF A 0 NoDelay A_SetRoll(roll+random(0,359))
    RMPF A 0 A_PlaySound("subweapon/energyclawdeflect",0,0.8,0)
    RMPF ABC 1
    Stop	
  XDeath:
    RMPF C 1
    Stop
  }
} 

ACTOR EnergyClawSlashFX
{	
  +NOGRAVITY
  +NOBLOCKMAP
  +THRUACTORS
  +FLOORCLIP
  +BRIGHT
  Radius 5
  Height 5
  RenderStyle ADD
  Alpha 0.45
  Scale 0.4
  States
  {
  Spawn:
     DRSL ABCD 1
	 Stop
  }
}

ACTOR EnergyClawSlashFXB
{	
  +NOGRAVITY
  +NOBLOCKMAP
  +THRUACTORS
  +FLOORCLIP
  +BRIGHT
  Radius 5
  Height 5
  RenderStyle ADD
  Alpha 0.45
  Scale 0.4
  States
  {
  Spawn:
     DRSL EFGH 1
	 Stop
  }
}

ACTOR EnergyXSlashLeft  : FastProjectile
{
   Speed 45
   Radius 15
   Height 15
   Damage (8)
   DamageType "SubWeapon"
   PROJECTILE
   +RIPPER
   +BLOODLESSIMPACT
   RenderStyle Add
   Alpha 0.75
   SeeSound "subweapon/energyclawproj"
   ReactionTime 6
   MissileHeight 8
   MissileType "EnergyXSlashLeftTail"
   States
   {
   Spawn:
      VTSD ABABABABABABABAB 2 Bright
	  Loop
   Death:
      VTSD B 2
	  VTSD A 0 A_FadeOut(0.15)
      Loop
   }
}

ACTOR EnergyXSlashLeftTail
{
	+THRUACTORS
	+NOCLIP
    +BRIGHT
	Radius 2
	Height 2
	Renderstyle AddStencil
	StencilColor "Purple"
	States
	{
   Spawn:
      VTSD AA 1 Bright
	  Stop
   Death:
      VTSD A 1 Bright
	  VTSD A 0 A_FadeOut(0.1)
      Loop  
	}
}

ACTOR EnergyXSlashRight : FastProjectile
{
   Speed 45
   Radius 15
   Height 15
   Damage (8)
   DamageType "SubWeapon"
   PROJECTILE
   +RIPPER
   +BLOODLESSIMPACT
   RenderStyle Add
   Alpha 0.75
   SeeSound "subweapon/energyclawproj"
   ReactionTime 6
   MissileHeight 8
   MissileType "EnergyXSlashRightTail"
   States
   {
   Spawn:
      VTSE ABABABABABABABAB 2 Bright
	  Loop
   Death:
      VTSE B 2
	  VTSE A 0 A_FadeOut(0.15)
      Loop
   }
}

ACTOR EnergyXSlashRightTail
{
	+THRUACTORS
	+NOCLIP
    +BRIGHT
	Radius 2
	Height 2
	Renderstyle AddStencil
	StencilColor "Purple"
	States
	{
   Spawn:
      VTSE AA 1 Bright
	  Stop
   Death:
      VTSE A 1 Bright
	  VTSE A 0 A_FadeOut(0.1)
      Loop  
	}
}

Actor EnergyClawSpark
{
   Speed 15
   Radius 4
   Height 4
   Scale 0.08
   Gravity 0.5
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   -NOGRAVITY
   +CLIENTSIDEONLY
   RenderStyle AddStencil
   StencilColor "Green"
   ReactionTime 12
   States
   {
   Spawn:
      SPTL A 2
	  SPTL A 0 A_CountDown
      Loop
   Death:
      TNT1 A 1
	  TNT1 A 0 A_FadeOut(0.15,1)
      Loop
   }
}

ACTOR SubEnergyClawCheck : Inventory
{
   -INVBAR
   +UNDROPPABLE
   Inventory.MaxAmount 3
   Inventory.InterHubAmount 3
   Inventory.Icon TNT1A0
}