//RISE OF DREAD
ACTOR BulletEyePatch1Title 5099
{
   +BRIGHT
   +THRUACTORS
   +NOGRAVITY
   +NOINTERACTION
   Radius 2
   Height 2
   Scale 0.4
   Alpha 1.0
   RenderStyle "Add"
   States
   {
   Spawn:
      BTTL A 0 NoDelay A_SetTranslucent(0.0)
	  BTTL A 120
   SpawnFall:  
      BTTL A 0 A_FadeIn(0.1,FTF_CLAMP)
      BTTL A 4
      Loop
   Death:
      TNT1 A 0
	  Stop
   }
}


ACTOR DreadUnit1 5100
{
   +BRIGHT
   +FLOATBOB
   +THRUACTORS
   +NOGRAVITY
   +NOINTERACTION
   +ROLLSPRITE
   Radius 2
   Height 2
   Scale 0.3
   States
   {
   Spawn:
      DTTL A 2
	  DTTL A 0 A_Jump(50,"SpawnLooped")
	  Loop
   SpawnLooped:
      DTTL A 4
	  DTTL A 0 A_SetRoll(1,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(2,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(3,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(2,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(1,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(0,SPF_INTERPOLATE,0)
	  DTTL A 12
	  DTTL A 0 A_SetRoll(-1,SPF_INTERPOLATE,0)	
	  DTTL A 4
	  DTTL A 0 A_SetRoll(-2,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(-3,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(-2,SPF_INTERPOLATE,0)
	  DTTL A 4
	  DTTL A 0 A_SetRoll(-1,SPF_INTERPOLATE,0)
	  DTTL A 12
	  DTTL A 0 A_SetRoll(0,SPF_INTERPOLATE,0)	  
	  Loop
   Death:
      TNT1 A 0
	  Stop
   }
}

ACTOR DreadUnit1Small : DreadUnit1 5101
{
   Scale 0.25
   FloatBobStrength 0.4
   States
   {
   Spawn:
      DTTL A 2
	  DTTL A 0 A_Jump(50,"SpawnLooped")
	  Loop
   SpawnLooped:
      DTTL A 6
	  DTTL A 0 A_SetRoll(1,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(2,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(3,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(2,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(1,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(0,SPF_INTERPOLATE,0)
	  DTTL A 14
	  DTTL A 0 A_SetRoll(-1,SPF_INTERPOLATE,0)	
	  DTTL A 6
	  DTTL A 0 A_SetRoll(-2,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(-3,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(-2,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(-1,SPF_INTERPOLATE,0)
	  DTTL A 14
	  DTTL A 0 A_SetRoll(0,SPF_INTERPOLATE,0)	  
	  Loop
   }
}

ACTOR DreadUnit1Big : DreadUnit1 5102
{
   Scale 0.5
   FloatBobStrength 1.5
   States
   {
   Spawn:
      DTTL A 2
	  DTTL A 0 A_Jump(50,"SpawnLooped")
	  Loop
   SpawnLooped:
   	  DTTL A 6
   	  DTTL A 0 A_SetRoll(0,SPF_INTERPOLATE,0)
      DTTL A 6
	  DTTL A 0 A_SetRoll(1,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(2,SPF_INTERPOLATE,0)
	  DTTL A 12
	  DTTL A 0 A_SetRoll(1,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(0,SPF_INTERPOLATE,0)
	  DTTL A 6
	  DTTL A 0 A_SetRoll(-1,SPF_INTERPOLATE,0)
	  DTTL A 6	  
	  DTTL A 0 A_SetRoll(-2,SPF_INTERPOLATE,0)
	  DTTL A 12
	  DTTL A 0 A_SetRoll(-1,SPF_INTERPOLATE,0)
	  DTTL A 6	
	  Loop
   }
}

ACTOR DreadVisageTitle 5103
{    
	Radius 31
	Height 56
	Scale 1.25
	+NOGRAVITY
	RenderStyle Stencil
	StencilColor "Black"
	States
	{
  Spawn:
    _DRD A 1
    _DRD AAA 0 A_SpawnItemEx("DreadVisageFragmentFX",frandom(-12,12),frandom(-12,12),frandom(-3,18),0,0)
	Loop	
  Death:
    _DRD D 1 A_FadeOut(0.15)
	Loop	   
	}
}

ACTOR TitleMoon 5105
{    
	Radius 15
	Height 15
	Scale 1
	+NOGRAVITY
	+BRIGHT
	States
	{
  Spawn:
    _MON A 1
	Loop	
  Death:
    _MON A 1
	Stop	   
	}
}

ACTOR TitleBranchGenerator1 5106
{    
	Radius 15
	Height 15
	Scale 1
	+NOGRAVITY
	+BRIGHT
	States
	{
  Spawn:
    TNT1 A 1
	TNT1 A 0 A_Jump(1,"BranchLeft")
	Loop
  BranchLeft:
    TNT1 A 0 A_CustomMissile("TitleBranchLeft", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Death:
    TNT1 A 1
	Stop	   
	}
}

ACTOR TitleBranchGenerator2 5107
{    
	Radius 15
	Height 15
	Scale 1
	+NOGRAVITY
	+BRIGHT
	States
	{
  Spawn:
    TNT1 A 1
	TNT1 A 0 A_Jump(1,"BranchRight")
	Loop
  BranchRight:
    TNT1 A 0 A_CustomMissile("TitleBranchRight", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Death:
    TNT1 A 1
	Stop	   
	}
}

ACTOR TitleBranchLeft
{    
	Radius 15
	Height 15
	Scale 0.2
	Speed 1
	RenderStyle "Normal"
	+NOGRAVITY
	+ROLLSPRITE
	+BRIGHT
	States
	{
  Spawn:
    _TBR A 0 NoDelay A_SetScale(frandom(0.2,0.4))
	_TBR A 0 A_SetRoll(roll+frandom(-10,10))
    _TBR A 20
	_TBR A 0 A_SetAngle(135)
	_TBR A 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)
	_TBR A 30
	_TBR A 0 A_SetAngle(160)
	_TBR A 0 A_ChangeVelocity (4, 0, -velz, CVF_RELATIVE|CVF_REPLACE)
	_TBR A 100	
	Stop	
  Death:
    _TBR A 0
    _TBR A 5
	_TBR A 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleBranchRight : TitleBranchLeft
{    
	Radius 15
	Height 15
	Scale 0.2
	Speed 1
	RenderStyle "Normal"
	+NOGRAVITY
	+ROLLSPRITE
	+BRIGHT
	+SPRITEFLIP
	States
	{
  Spawn:
    _TBR A 0 NoDelay A_SetScale(frandom(0.2,0.4))
	_TBR A 0 A_SetRoll(roll+frandom(-10,10))
    _TBR A 20
	_TBR A 0 A_SetAngle(45)
	_TBR A 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)
	_TBR A 30
	_TBR A 0 A_SetAngle(25)
	_TBR A 0 A_ChangeVelocity (4, 0, -velz, CVF_RELATIVE|CVF_REPLACE)
	_TBR A 100	
	Stop	
  Death:
    _TBR A 0
    _TBR A 5
	_TBR A 0 A_FadeOut(0.2)
	Loop	   
	}
}

ACTOR TitleTreeGenerator 5108
{    
	Radius 15
	Height 15
	Scale 1
	+NOGRAVITY
	+BRIGHT
	States
	{
  Spawn:
    TNT1 A 1
	TNT1 A 0 A_Jump(5,"Tree1","Tree2","Tree3")
	Loop
  Tree1:
    TNT1 A 0 A_CustomMissile("TitleTreeVar1", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Tree2:
    TNT1 A 0 A_CustomMissile("TitleTreeVar2", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Tree3:
    TNT1 A 0 A_CustomMissile("TitleTreeVar2", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn	
  Death:
    TNT1 A 1
	Stop	   
	}
}

ACTOR TitleTreeVar1
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 1
	RenderStyle "Normal"
	+NOGRAVITY
	+NOCLIP
	+BRIGHT
	ReactionTime 100
	States
	{
  Spawn:
    _TTR A 0 NoDelay A_SetScale(frandom(0.9,1.2))
    _TTR A 100
  SpawnFall:
    _TTR A 3
	_TTR A 0 A_SetScale(scalex-0.01)
	_TTR A 0 ThrustThingZ(0,1,1,0)  
    _TTR A 0 A_CountDown
	Loop
  Death:
    _TTR A 0 A_SetScale(scalex-0.01)
	_TTR A 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR A 2
	_TTR A 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeVar2
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 1
	RenderStyle "Normal"
	+NOGRAVITY
	+NOCLIP
	+BRIGHT
	ReactionTime 100
	States
	{
  Spawn:
    _TTR B 0 NoDelay A_SetScale(frandom(0.9,1.2))
    _TTR B 100
  SpawnFall:
    _TTR B 3
	_TTR B 0 A_SetScale(scalex-0.01)
	_TTR B 0 ThrustThingZ(0,1,1,0)  
    _TTR B 0 A_CountDown
	Loop	
  Death:
    _TTR B 0 A_SetScale(scalex-0.01)
	_TTR B 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR B 2
	_TTR B 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeVar3
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 1
	RenderStyle "Normal"
	+NOGRAVITY
	+NOCLIP
	+BRIGHT
	ReactionTime 100
	States
	{
  Spawn:
    _TTR C 0 NoDelay A_SetScale(frandom(0.9,1.2))
    _TTR C 100
  SpawnFall:
    _TTR C 3
	_TTR C 0 A_SetScale(scalex-0.01)
	_TTR C 0 ThrustThingZ(0,1,1,0)  
    _TTR C 0 A_CountDown
	Loop
  Death:
    _TTR C 0 A_SetScale(scalex-0.01)
	_TTR C 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR C 2
	_TTR C 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeTopGeneratorLeft 5109
{    
	Radius 15
	Height 15
	Scale 1
	+NOGRAVITY
	+BRIGHT
	States
	{
  Spawn:
    TNT1 A 1
	TNT1 A 0 A_Jump(5,"Tree1","Tree2","Tree3")
	Loop
  Tree1:
    TNT1 A 0 A_CustomMissile("TitleTreeVarRotL1", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Tree2:
    TNT1 A 0 A_CustomMissile("TitleTreeVarRotL2", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Tree3:
    TNT1 A 0 A_CustomMissile("TitleTreeVarRotL3", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn	
  Death:
    TNT1 A 1
	Stop	   
	}
}

ACTOR TitleTreeVarRotL1
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 2
	RenderStyle "Normal"
	+NOGRAVITY
	+ROLLSPRITE
	+NOCLIP
	+BRIGHT
	ReactionTime 20
	States
	{
  Spawn:
    _TTR A 0 NoDelay A_SetScale(frandom(0.9,1.2))
	_TTR C 0 A_SetRoll(roll+frandom(-30,-60))
    _TTR A 100
  SpawnFall:
    _TTR A 10
	_TTR A 0 A_SetScale(scalex-0.01)
	_TTR A 0 A_SetAngle(160)
	_TTR A 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)	
	_TTR A 0 ThrustThingZ(0,1,1,0)  
    _TTR A 0 A_CountDown
	Loop
  Death:
    _TTR A 0 A_SetScale(scalex-0.01)
	_TTR A 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR A 2
	_TTR A 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeVarRotL2
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 2
	RenderStyle "Normal"
	+NOGRAVITY
	+ROLLSPRITE
	+NOCLIP
	+BRIGHT
	ReactionTime 20
	States
	{
  Spawn:
    _TTR B 0 NoDelay A_SetScale(frandom(0.9,1.2))
	_TTR C 0 A_SetRoll(roll+frandom(-30,-60))
    _TTR B 100
  SpawnFall:
    _TTR B 10
	_TTR B 0 A_SetScale(scalex-0.01)
	_TTR B 0 A_SetAngle(160)
	_TTR B 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)	
	_TTR B 0 ThrustThingZ(0,1,1,0)  
    _TTR B 0 A_CountDown
	Loop	
  Death:
    _TTR B 0 A_SetScale(scalex-0.01)
	_TTR B 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR B 2
	_TTR B 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeVarRotL3
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 2
	RenderStyle "Normal"
	+NOGRAVITY
	+NOCLIP
	+ROLLSPRITE
	+BRIGHT
	ReactionTime 20
	States
	{
  Spawn:
    _TTR C 0 NoDelay A_SetScale(frandom(0.9,1.2))
	_TTR C 0 A_SetRoll(roll+frandom(-30,-60))
    _TTR C 100
  SpawnFall:
    _TTR C 10
	_TTR C 0 A_SetScale(scalex-0.01)
	_TTR C 0 A_SetAngle(160)
	_TTR C 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)
	_TTR C 0 ThrustThingZ(0,1,1,0)  
    _TTR C 0 A_CountDown
	Loop
  Death:
    _TTR C 0 A_SetScale(scalex-0.01)
	_TTR C 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR C 2
	_TTR C 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeTopGeneratorRight 5110
{    
	Radius 15
	Height 15
	Scale 1
	+NOGRAVITY
	+BRIGHT
	States
	{
  Spawn:
    TNT1 A 1
	TNT1 A 0 A_Jump(5,"Tree1","Tree2","Tree3")
	Loop
  Tree1:
    TNT1 A 0 A_CustomMissile("TitleTreeVarRotR1", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Tree2:
    TNT1 A 0 A_CustomMissile("TitleTreeVarRotR2", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn
  Tree3:
    TNT1 A 0 A_CustomMissile("TitleTreeVarRotR3", 15, 0, 0, CMF_AIMDIRECTION)
	Goto Spawn	
  Death:
    TNT1 A 1
	Stop	   
	}
}

ACTOR TitleTreeVarRotR1
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 2
	RenderStyle "Normal"
	+NOGRAVITY
	+ROLLSPRITE
	+SPRITEFLIP
	+NOCLIP
	+BRIGHT
	ReactionTime 20
	States
	{
  Spawn:
    _TTR A 0 NoDelay A_SetScale(frandom(0.9,1.2))
	_TTR C 0 A_SetRoll(roll+frandom(30,60))
    _TTR A 100
  SpawnFall:
    _TTR A 3
	_TTR A 0 A_SetScale(scalex-0.01)
	_TTR A 0 A_SetAngle(25)
	_TTR A 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)	
	_TTR A 0 ThrustThingZ(0,1,1,0)  
    _TTR A 0 A_CountDown
	Loop
  Death:
    _TTR A 0 A_SetScale(scalex-0.01)
	_TTR A 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR A 2
	_TTR A 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeVarRotR2
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 2
	RenderStyle "Normal"
	+NOGRAVITY
	+ROLLSPRITE
	+SPRITEFLIP
	+NOCLIP
	+BRIGHT
	ReactionTime 20
	States
	{
  Spawn:
    _TTR B 0 NoDelay A_SetScale(frandom(0.9,1.2))
	_TTR C 0 A_SetRoll(roll+frandom(30,60))
    _TTR B 100
  SpawnFall:
    _TTR B 3
	_TTR B 0 A_SetScale(scalex-0.01)
	_TTR B 0 A_SetAngle(25)
	_TTR B 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)	
	_TTR B 0 ThrustThingZ(0,1,1,0)  
    _TTR B 0 A_CountDown
	Loop	
  Death:
    _TTR B 0 A_SetScale(scalex-0.01)
	_TTR B 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR B 2
	_TTR B 0 A_FadeOut(0.2)	
	Loop	   
	}
}

ACTOR TitleTreeVarRotR3
{    
	Radius 15
	Height 15
	Scale 1.2
	Speed 2
	RenderStyle "Normal"
	+NOGRAVITY
	+NOCLIP
	+ROLLSPRITE
	+SPRITEFLIP
	+BRIGHT
	ReactionTime 20
	States
	{
  Spawn:
    _TTR C 0 NoDelay A_SetScale(frandom(0.9,1.2))
	_TTR C 0 A_SetRoll(roll+frandom(30,60))
    _TTR C 100
  SpawnFall:
    _TTR C 3
	_TTR C 0 A_SetScale(scalex-0.01)
	_TTR C 0 A_SetAngle(25)
	_TTR C 0 A_ChangeVelocity (2, 0, -velz, CVF_RELATIVE|CVF_REPLACE)	
	_TTR C 0 ThrustThingZ(0,1,1,0)  
    _TTR C 0 A_CountDown
	Loop
  Death:
    _TTR C 0 A_SetScale(scalex-0.01)
	_TTR C 0 A_SetRenderStyle(0.2,STYLE_Shadow)
    _TTR C 2
	_TTR C 0 A_FadeOut(0.2)	
	Loop	   
	}
}