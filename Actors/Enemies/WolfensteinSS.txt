ACTOR WolfensteinSSMM : BEEnemyBase
{
    Health 50
    Radius 20
    Height 56
    Speed 8
    PainChance 170
    Monster
    +FLOORCLIP
	+NOEXTREMEDEATH
    SeeSound "wolfss/sight"
    PainSound "wolfss/pain"
    DeathSound "wolfss/death"
    ActiveSound "wolfss/active"
    AttackSound "wolfss/attack"
    Obituary "$OB_WOLFSS"
	Tag "Wolfenstein SS"
	
	StencilColor "Red"
	
	BloodType "BEPatentedFakeBlood"
	
	PainChance "BlazeStreamSlowBurn", 256
	PainChance "ScrapLauncherChain", 256
	PainChance "MetalMonstrosity", 256
 
	//Enemy Specific
	DamageFactor "AlphaVenom", 3.0	

    DropItem EnemyDropLowPool 50
	DropItem StrikeOrbPool 10
    States
	{
  Spawn:
    SSWV AB 10 A_Look
    Loop	
  See:
	SSWV A 0 A_TakeInventory("BusterEnemyAreaHit",1)
    SSWV AABBCCDD 3 A_Chase
	SSWV A 0 A_GiveInventory("EnemyAware",1)
	SSWV A 0 A_JumpIfInTargetInventory("AssassinVulcanJustFiredAlt",1,"SeeAssassinReset")
    Loop
  SeeAssassinReset:
    SSWV A 0 A_ClearTarget
	SSWV A 0 A_TakeInventory("EnemyAware",2)
	Goto Alerted
  Missile:
    SSWV E 10 A_FaceTarget
    SSWV F 10 A_FaceTarget
    SSWV G 4 Bright A_CPosAttack
    SSWV F 6 A_FaceTarget
    SSWV G 4 Bright A_CPosAttack
    SSWV F 1 A_CPosRefire
    Goto Missile+2
  Pain:
    SSWV H 3
    SSWV H 3 A_Pain
    Goto See	
  Stunned:
    SSWV H 30
	Goto See
  ShockwaveStunned:
    SSWV H 10
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV H 10
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV H 10
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV H 10
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	SSWV H 10
	Goto See	
  Death:    
	SSWV AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    SSWV I 8
	SSWV A 0 A_NoBlocking
	SSWV AAA 0 A_DropItem("ScorePoolSmall",1,30)
	SSWV A 0 A_JumpIf(GetCvar("bulleteye_oldsystemweapdrops") == 0,2)
	SSWV A 0 A_DropItem("LowGradeWeaponPool",1,5)
	SSWV A 0 A_DropAmmoForWeapons(60)
	SSWV A 0 A_DropItem("AmmoLockboxSmall",1,6)
	SSWV AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	SSWV AAAAA 0 A_SpawnItemEx("EnemyDeathExplosionExtra",0,0,8,frandom(-4,4),frandom(-4,4),frandom(3,7),frandom(0,359),32)
    SSWV J 8 A_Scream
	SSWV AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    SSWV K 6
	SSWV AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    SSWV K 1
	TNT1 A 2
	SSWV AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	SSWV K 2
    TNT1 A 2
	SSWV AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	SSWV K 2
    TNT1 A 0 A_ChangeFlag("NOGRAVITY",0)
    TNT1 A -1	
  XDeath:
    SSWV N 5 
    SSWV O 5 A_XScream
    SSWV P 5 A_NoBlocking
	SSWV AA 0 A_DropItem("ScorePoolSmall",1,30)
	SSWV A 0 A_JumpIf(GetCvar("bulleteye_oldsystemweapdrops") == 0,2)
	SSWV A 0 A_DropItem("LowGradeWeaponPool",1,5)
	SSWV A 0 A_DropAmmoForWeapons(60)	
    SSWV V -1
    Stop
   }
}