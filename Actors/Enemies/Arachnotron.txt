Actor ArachnotronSpawner : GeneralDummySpawner
{

	States
	{
	Spawn:
		TNT1 A 0
		TNT1 A 0 A_SpawnItemEx("ArachnotronMM",0,0,0,0,0,0,0,SXF_SETMASTER|SXF_NOCHECKPOSITION|SXF_TRANSFERAMBUSHFLAG)
		Goto Waiting
	}
}

ACTOR ArachnotronMM : BEEnemyBase
{
    Health 500
    Radius 64 //35
    Height 64
    Mass 600
    Speed 12
    PainChance 128
    MONSTER
    +FLOORCLIP
    +BOSSDEATH
    SeeSound "baby/sight"
    PainSound "baby/pain"
    DeathSound "baby/death"
    ActiveSound "baby/active"
    Obituary "$OB_BABY"
	Tag "Arachnotron"

	StencilColor "Red"
	
	BloodType "BEPatentedFakeBlood"

    PainChance "SubWeapon", 200
	PainChance "QuickStrike", 150
	PainChance "BlazeStreamSlowBurn", 256
	PainChance "ScrapLauncherChain", 256
	PainChance "MetalMonstrosity", 256
	
	DamageFactor "Spitfire", 1.2
	DamageFactor "SpreadShot", 1.0
	DamageFactor "Buster", 1.3
	DamageFactor "BlazeStream", 1.2
	DamageFactor "ChaserMissile", 1.3
	DamageFactor "CrushBlast", 1.2
	DamageFactor "ScrapLauncher", 1.1
    DamageFactor "QuickStrike", 1.4
	DamageFactor "HuntingLaser", 1.0
	DamageFactor "RapidVulcan", 1.5
	DamageFactor "StormGrenade", 1.0
	DamageFactor "VolleyShot", 1
	DamageFactor "ViperWave", 1.4
	DamageFactor "Repeater", 1.2
	DamageFactor "TripleWyvern", 1.2
	DamageFactor "WingedReaver", 1.2
	DamageFactor "SubWeapon", 1.2
	DamageFactor "BulletEye", 1.8
	//Enemy Specific
	DamageFactor "AlphaVenom", 5.0		
	
    DropItem EnemyDropHighPool 140
	DropItem StrikeOrbPool 30
	DropItem GearPool 8
	States
	{
  Spawn:
    BSPI AB 10 A_Look
    Loop	
  See:
    BSPI A 20
	BSPI A 0 A_TakeInventory("BusterEnemyAreaHit",1)
    BSPI A 3 A_BabyMetal
    BSPI ABBCC 3 A_Chase
    BSPI D 3 A_BabyMetal
    BSPI DEEFF 3 A_Chase
	BSPI A 0 A_GiveInventory("EnemyAware",1)
	BSPI A 0 A_JumpIfInTargetInventory("AssassinVulcanJustFiredAlt",1,"SeeAssassinReset")
    Goto See+1
  SeeAssassinReset:
    BSPI A 0 A_ClearTarget
	BSPI A 0 A_TakeInventory("EnemyAware",2)
	Goto Alerted
  Missile:
    BSPI A 20 Bright A_FaceTarget
    BSPI G 4 Bright A_BspiAttack
    BSPI H 4 Bright
    BSPI H 1 Bright A_SpidRefire
    Goto Missile+1
  Stunned:
    BSPI I 30
	Goto See+1
  ShockwaveStunned:
    BSPI I 10
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI I 10
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI I 10
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI I 10
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	BSPI I 10
	Goto See+1
  Pain:
    BSPI I 1 A_Pain	
	BSPI A 0 SecondaryPainSound()
	BSPI I 4 Bright
    Goto See+1
  Death:
	BSPI AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    BSPI J 8
	BSPI A 0 A_NoBlocking
	BSPI AAA 0 A_DropItem("ScorePoolMedium",1,30)
	BSPI A 0 A_JumpIf(GetCvar("bulleteye_oldsystemweapdrops") == 0,2)
	BSPI A 0 A_DropItem("HighGradeWeaponPool",1,5)
	BSPI A 0 A_DropAmmoForWeapons(90)
	BSPI A 0 A_DropItem("AmmoLockboxSmall",1,6)
	BSPI A 0 A_DropItem("AmmoLockboxLarge",1,5)
	BSPI AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	BSPI AAAAA 0 A_SpawnItemEx("EnemyDeathExplosionExtra",0,0,8,frandom(-4,4),frandom(-4,4),frandom(3,7),frandom(0,359),32)
	BSPI A 0 A_SpawnItemEx("EnemyDeathExplosionBig",random(-16,16),random(-16,16),random(8,65),0,0,0,0)
    BSPI K 8 A_Scream
	BSPI AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	BSPI A 0 A_SpawnItemEx("EnemyDeathExplosionBig",random(-16,16),random(-16,16),random(8,65),0,0,0,0)
    BSPI L 6
	BSPI AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	BSPI A 0 A_SpawnItemEx("EnemyDeathExplosionBig",random(-16,16),random(-16,16),random(8,65),0,0,0,0)
    BSPI M 1
	TNT1 A 2
	BSPI AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	BSPI A 0 A_SpawnItemEx("EnemyDeathExplosionBig",random(-16,16),random(-16,16),random(8,65),0,0,0,0)
	BSPI M 2
    TNT1 A 2
	BSPI AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	BSPI A 0 A_SpawnItemEx("EnemyDeathExplosionBig",random(-16,16),random(-16,16),random(8,65),0,0,0,0)
	BSPI M 2
	TNT1 A 0 A_ChangeFlag("NOGRAVITY",0)
    TNT1 A 1 A_BossDeath
	TNT1 A 0 A_CheckPlayerDone
    Stop
  Raise:
    BSPI P 5
    BSPI ONMLKJ 5
    Goto See+1	
   }
}