ACTOR DoomImpMM : BEEnemyBase
{
    Health 60
    Radius 20
    Height 56
    Mass 100
    Speed 10
    PainChance 200
    Monster
    +FLOORCLIP
	+NOEXTREMEDEATH
    SeeSound "imp/sight"
    PainSound "imp/pain"
    DeathSound "imp/death"
    ActiveSound "imp/active"
    HitObituary "$OB_IMPHIT"
    Obituary "$OB_IMP"
	Tag "Imp"

    StencilColor "Red"
	
	BloodType "BEPatentedFakeBlood"
    
	PainChance "SubWeapon", 200
	PainChance "QuickStrike", 150
	PainChance "BlazeStreamSlowBurn", 256
	PainChance "ScrapLauncherChain", 256
	PainChance "MetalMonstrosity", 256
	
	DamageFactor "BlazeStream", 1.15
	//Enemy Specific
	DamageFactor "AlphaVenom", 3.0

    DropItem EnemyDropLowPool 65
	DropItem StrikeOrbPool 10
	States
	{
  Spawn:
    TROO AB 10 A_Look
    Loop	
  See:
	TROO A 0 A_TakeInventory("BusterEnemyAreaHit",1)
    TROO AABBCCDD 3 A_Chase
	TROO A 0 A_GiveInventory("EnemyAware",1)
	TROO A 0 A_JumpIfInTargetInventory("AssassinVulcanJustFiredAlt",1,"SeeAssassinReset")
    Loop
  SeeAssassinReset:
    TROO A 0 A_ClearTarget
	TROO A 0 A_TakeInventory("EnemyAware",2)
	Goto Alerted
  Melee:
  Missile:
    TROO EF 8 A_FaceTarget
    TROO G 6 A_CustomComboAttack("DoomImpBall", 32, 3 * random(1, 8), "imp/melee", "EnemyMelee") //A_TroopAttack
    Goto See
  Stunned:
    TROO H 30
	Goto See
  ShockwaveStunned:
    TROO H 10
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO H 10
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO H 10
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO H 10
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),frandom(1,45),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-15,2,2,2,0,SXF_NOCHECKPOSITION)
	TROO AAA 0 A_SpawnItemEx("ShockwaveStunElectricFX",frandom(-8,8),frandom(-8,8),height-frandom(15,20),2,2,2,0,SXF_NOCHECKPOSITION)
	TROO H 10
	Goto See
  Pain:
    TROO H 1 A_Pain
	TROO A 0 SecondaryPainSound()
	TROO H 4 Bright
    Goto See
  Death:
	TROO AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    TROO I 8
	TROO A 0 A_NoBlocking
	TROO AAA 0 A_DropItem("ScorePoolSmall",1,30)
	TROO A 0 A_JumpIf(GetCvar("bulleteye_lowtiermonstgear") == 0,2)
	TROO A 0 A_DropItem("SuperGearPool",1,2)
	TROO A 0 A_JumpIf(GetCvar("bulleteye_oldsystemweapdrops") == 0,2)
	TROO A 0 A_DropItem("LowGradeWeaponPool",1,5)
	TROO A 0 A_DropAmmoForWeapons(60)
	TROO A 0 A_DropItem("AmmoLockboxSmall",1,6)
	TROO AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	TROO AAAAA 0 A_SpawnItemEx("EnemyDeathExplosionExtra",0,0,8,frandom(-4,4),frandom(-4,4),frandom(3,7),frandom(0,359),32)
    TROO J 8 A_Scream
	TROO AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    TROO K 6
	TROO AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
    TROO K 1
	TNT1 A 2
	TROO AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	TROO K 2
    TNT1 A 2
	TROO AA 0 A_SpawnItemEx("EnemyDeathExplosion",random(-12,12),random(-12,12),random(8,65),0,0,0,0)
	TROO K 2
	TNT1 A 0 A_ChangeFlag("NOGRAVITY",0)
	TNT1 A 0 A_CheckPlayerDone
    TNT1 A -1
    Stop
  Raise:
    TROO ML 8
    TROO KJI 6
    Goto See	
   }
}