ACTOR BulletEyeHeal : Health
{
  Inventory.Amount 8
}

ACTOR PowerUpMediGemSmall : Health replaces Stimpack
{
  Inventory.Amount 15 
  Inventory.PickupMessage "Small Medi-Gem"
  Inventory.PickupSound "powerup/medigem" 
  Scale 1.2
  Radius 20
  States
  {
  Spawn:
    PHLS A 0 NoDelay A_GiveInventory("ItemFXCheckDummy",1)
	PHLS A 0 A_SpawnItemEx("ItemShineFX",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION)
  SpawnFall:
    PHLS AB 3
    Loop
  }
}

ACTOR PowerUpMediGemLarge : Health replaces Medikit
{
  Inventory.Amount 30
  Inventory.PickupMessage "Large Medi-Gem"
  Inventory.PickupSound "powerup/medigem"  
  Radius 20
  States
  {
  Spawn:
    PHLG A 0 NoDelay A_GiveInventory("ItemFXCheckDummy",1)
	PHLG A 0 A_SpawnItemEx("ItemShineFX",0,0,0,0,0,0,0,SXF_NOCHECKPOSITION)
  SpawnFall:
    PHLG AB 3
    Loop
  }
}

Actor MediGemPool : RandomSpawner //For Item Capsule low health prioritization.
{
	DropItem "PowerUpMediGemSmall" 255 10
	DropItem "PowerUpMediGemLarge" 255 4
}

Actor HealthAugment : Health
{
  +COUNTITEM
  +INVENTORY.ALWAYSPICKUP
  +BRIGHT
  Inventory.PickupMessage "Health Augment"  
  Inventory.PickupSound "powerup/healthaug"   
  Inventory.Amount 2
  Inventory.MaxAmount 300
  Radius 20
  Height 10
  States
  {
  Spawn:
    HAUG ABC 2
    Loop
  }
}  

Actor HealthAugmentFG : HealthAugment //Flash Guard Health Augment.
{
  -COUNTITEM
}  

//FOOD ITEMS

ACTOR FoodHotDog : Health
{
  Inventory.Amount 15 
  Inventory.PickupMessage "Hot Dog"
  Inventory.PickupSound "food/hotdogget" 
  Scale 0.5
  Radius 20
  States
  {
  Spawn:
    HDOG A 1
    Loop
  }
}