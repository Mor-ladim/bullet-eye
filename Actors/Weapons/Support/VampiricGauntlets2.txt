//MECHANICS
ACTOR VampiricGauntletsWeaponS2 : VampiricGauntletsWeapon
{
   Weapon.SlotNumber 2
   Tag "Vampiric Gauntlets"
   States
   {
   Deselect:
      TNT1 AA 0 A_Lower
      _WVG A 1 A_Lower
      Loop  
   Select:
	  _WVG A 0 A_StopSound(5)
      _WVG A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:
      TNT1 AA 0 A_Raise   
      _WVG A 1 A_Raise
      Loop 
   }
}