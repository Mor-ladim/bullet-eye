//MECHANICS
ACTOR WingedReaverWeaponS2 : WingedReaverWeapon
{  
   Weapon.SlotNumber 2
   Tag "Winged Reaver"
   States
   {
   Deselect:
      TNT1 AA 0 A_Lower
      _WWR A 1 A_Lower
      Loop  
   Select:
	  TNT1 A 0 A_StopSound(5)
      _PRP A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:
      TNT1 AA 0 A_Raise   
      _WWR A 1 A_Raise
      Loop 
   }
}