//MECHANICS
ACTOR ScrapLauncherWeapon : BEWeaponBase
{
   Inventory.PickupSound "misc/w_pkup"
   Inventory.PickupMessage ""
   Inventory.Icon "WEAP07"
   Weapon.SlotNumber 1
   Weapon.BobStyle Inverse
   Weapon.BobSpeed 2.3
   Weapon.BobRangeX 0.5
   Weapon.BobRangeY 0.3   
   Weapon.AmmoType1 "AmmoScrapLauncher"
   Weapon.AmmoType2 "AmmoPowerEnergy"
   Weapon.UpSound "beplayer/weapswitchscraplauncher"
   BEWeaponBase.WeaponPowerupItem "NewScrapLauncherPowerup"
   BEWeaponBase.ReloadProperties "weapons/scraplauncherreloadstage1", "weapons/scraplauncherreloadstage2", "weapons/scraplauncherreloadstage3"
   BEWeaponBase.Sprite "WEAPH07"
   BEWeaponBase.Type 0
   BEWeaponBase.Attributes 3, 4, 1, 4
   BEWeaponBase.EffectTexts "- Launches out scrap in a spread. Alt-fire launches a chain to pull in enemies.", "- Enemies that are pulled and slain within 2 seconds add ammo into the magazine. Increases chain range.", "- Pressing Alt-fire a second time after pulling in an enemy will detach the chain and pull or knock back enemies. After impact, the chain links scatter into penetrating scrap."
   BEWeaponBase.FlavorText "A jury-rigged weapon designed for self-defense by junkyard workers, the scrap launcher...launches scrap. Life in the 'yard requires a keen sense of mechanics and engineering, and this gun might be hefty but it packs a punch."
   +WEAPON.ALT_AMMO_OPTIONAL
   +WEAPON.NO_AUTO_SWITCH
   +WEAPON.CHEATNOTWEAPON
   Tag "Scrap Launcher"   
   States
   {
   Spawn:
      _WSL A -1
      Loop
   Ready:
      _WSL A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4)
      Loop
	  _WSL A 1 Offset( 0, 80) //Head here after Sub-Weapon usage.
	  _WSL A 1 Offset( 0, 60)
	  _WSL A 1 Offset( 0, 43)
	  TNT1 A 0 A_WeaponReady(WRF_NOFIRE|WRF_NOSWITCH)
	  Goto Ready	  
   Deselect:
      TNT1 AA 0 A_Lower
      _WSL A 1 A_Lower
      Loop  
   Select:
	  _WSL A 0 A_StopSound(5)
      _WSL A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:	
      TNT1 AA 0 A_Raise
      _WSL A 1 A_Raise
      Loop 
  Fire: //Launches out scrap in a spread.
      _WSL A 0 A_CheckWeaponFire
	  _WSL B 1 Bright A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_PlaySound("weapons/scraplaunchershoot",CHAN_WEAPON,0.8,0)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)	  
	  _WSL A 0 A_AlertMonsters	
	  _WSL A 0 Radius_Quake(2, 2, 0, 2, 0)
	  _WSL AAA 0 A_SpawnItemEx("ScrapLauncherExtraFXA", 30, 0, 35, 0, 0, 0, 0, SXF_NOCHECKPOSITION)
	  _WSL AAA 0 A_SpawnItemEx("ScrapLauncherExtraFXB", 30, 0, 35, 0, 0, 0, 0, SXF_NOCHECKPOSITION)
	  _WSL AAA 0 A_SpawnItemEx("ScrapLauncherExtraFXC", 30, 0, 35, 0, 0, 0, 0, SXF_NOCHECKPOSITION)
	  _WSL AAAAAAAAAA 0 A_FireBEProjectile("ScrapProjectile", frandom(-8, 8), 0, 0, 0, FPF_NOAUTOAIM, frandom(-6, 4))
	  _WSL A 0 A_TakeInventory("ScrapLauncherFirerateBonus",1)
	  _WSL A 0 A_TakeAmmo
	  _WSL A 0 A_TakeDurability(1)
	  _WSL A 0 A_GunFlash	  
	  _WSL F 2 Offset( 0, 38)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_SetPitch(pitch-1.5)
	  _WSL F 1 Offset( 0, 46)
	  _WSL A 0 A_SetPitch(pitch-0.5)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL E 1 Offset( 0, 43)
	  _WSL A 0 A_SetPitch(pitch+0.5)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL E 1 Offset( 0, 42)	  
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL G 1 Offset( 0, 38)
	  _WSL A 0 A_SetPitch(pitch+1.5)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL H 1 Offset( 0, 36)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL H 1 Offset( 0, 35)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL H 1 Offset( 0, 32)
	  _WSL A 0 A_PlaySound("weapons/scraplauncherfireextra",CHAN_AUTO,0.8,0)
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL HIAHIAHIA 1 //Offset( 0, 33)
	  _WSL IHIA 2
	  _WSL A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  //_WSL C 1 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 12 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4)
	  _WSL A 1 A_ReFire	  
      Goto Ready  
  Altfire:
      _WSL A 0 A_JumpIf(A_GetWeaponLevel() == 3,"AltfireL3")
      _WSL A 0 A_JumpIf(A_GetWeaponLevel() == 2,"AltfireL2")
      _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"AltfireCooldown")	  
      _WSL A 0 A_FireProjectile("ScrapLauncherChain", 0, 0, 0, 0, FPF_NOAUTOAIM, 0)
	  _WSL A 0 A_PlaySound("weapons/scraplauncherchainpool",CHAN_WEAPON,1.0,0)
	  _WSL A 0 A_TakeDurability(1)
	  _WSL IHE 2
	  _WSL HI 1
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4)
	  Goto Ready
  AltfireCooldown:
      _WSL A 20
	  _WSL A 0 A_TakeInventory("ScrapLauncherFirerateBonus")
	  Goto Ready
  AltfireCooldownL3:
      _WSL A 10 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_TakeInventory("ScrapLauncherFirerateBonus")
	  Goto Ready	  
  AltfireL2:
      _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"AltfireCooldown")
      _WSL A 0 A_FireProjectile("ScrapLauncherChainL2", 0, 0, 0, 0, FPF_NOAUTOAIM, 0)
	  _WSL A 0 A_PlaySound("weapons/scraplauncherchainpool",CHAN_WEAPON,1.0,0)
	  _WSL A 0 A_TakeDurability(1)
	  _WSL CHF 2
	  _WSL EG 1
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4)
	  Goto Ready
  AltfireL3:
      _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Launch")
      _WSL A 0 A_FireProjectile("ScrapLauncherChainL2", 0, 0, 0, 0, FPF_NOAUTOAIM, 0)
	  _WSL A 0 A_PlaySound("weapons/scraplauncherchainpool",CHAN_WEAPON,1.0,0)
	  _WSL A 0 A_TakeDurability(1)
	  _WSL CHF 2
	  _WSL EG 1
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSL A 0 A_JumpIfInventory("ScrapLauncherFirerateBonus",1,"Ready")
	  _WSL A 5 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4)
	  Goto Ready
  Launch:
      _WSL A 0 A_CheckWeaponFire
      _WSL A 0 A_FireProjectile("ScrapChainLaunched", 0, 0, 0, 0, FPF_NOAUTOAIM, 0)
	  _WSL A 0 A_PlaySound("weapons/scraplauncherchainpool",CHAN_WEAPON,1.0,0)
	  _WSL A 0 A_TakeAmmo
	  _WSL A 0 A_TakeDurability(1)
	  _WSL CHF 2
	  _WSL EG 1
	  _WSL A 0 A_TakeInventory("ScrapLauncherFirerateBonus",1)
	  _WSL A 20 A_WeaponReady(WRF_NOPRIMARY|WRF_NOSECONDARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  Goto Ready
   Flash:
      TNT1 A 1 Bright A_Light1
	  TNT1 A 1 Bright A_Light0
      Stop
   }
}

ACTOR ScrapProjectile : BEPlayerProjectile
{
   Radius 10
   Height 5
   Speed 65
   Damage (16)
   DamageType "ScrapLauncher"   
   PROJECTILE
   +BRIGHT
   +THRUSPECIES
   +ROLLSPRITE
   Species "Player"   
   Scale 0.2
   DeathSound "weapons/scraplauncherprojpool"
   States
   {
   Spawn:
      SLPJ A 0
	  SLPJ A 0 A_Jump(256,"Var1","Var2","Var3","Var4")
      Loop
   Var1:
      SLPJ A 1
	  SLPJ A 0 A_SetRoll(roll+20) 
	  Loop
   Var2:
      SLPJ B 1
	  SLPJ A 0 A_SetRoll(roll+20)
	  SLPJ A 0 A_SetScale(0.15)
	  Loop
   Var3:
      SLPJ C 1
	  SLPJ A 0 A_SetRoll(roll+20)
	  Loop
   Var4:
      SLPJ D 1
	  SLPJ A 0 A_SetRoll(roll+20)
	  SLPJ A 0 A_SetScale(0.15)
	  Loop	  
   Death:
      SPRI A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SPRI A 0 A_SetRoll(roll+random(0,359))
	  SPRI A 0 A_SpawnItemEx("ScrapImpactSweetener",0,0,0,0,0,0)
      SPRI ABCDE 1
	  Stop
   } 
}

ACTOR ScrapImpactSweetener
{ 
    Scale 0.5
    +NOBLOCKMAP
    +NOGRAVITY
    +PUFFONACTORS
	+ROLLSPRITE
	+BRIGHT
    Renderstyle Add
    Alpha 0.4
    States
    {
    Spawn:
	  RPPF A 0 NoDelay A_SetRoll(random(0,359))
	  RPPF ABCDE 1
	  Stop
    }
}

ACTOR ScrapProjectileX : BEPlayerProjectile //L2 Projectile
{
   Radius 10
   Height 5
   Speed 65
   DamageType "ScrapLauncher"  
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +BLOODLESSIMPACT
   +THRUSPECIES
   +ROLLSPRITE
   Species "Player"   
   Scale 0.2
   DeathSound "weapons/scraplauncherprojpool"   
   MissileHeight 8
   MissileType "ScrapTrail"
   States
   {
   Spawn:
      SLPJ A 0
	  SLPJ A 0 A_Jump(256,"Var1","Var2","Var3","Var4")
      Loop
   Var1:
      SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  SLPJ A 0 A_Explode(4,15,0,0,6)
      SLPJ A 1
	  SLPJ A 0 A_Explode(4,15,0,0,6)
	  SLPJ A 0 A_SetRoll(roll+20)
	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  Loop
   Var2:
   	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  SLPJ A 0 A_Explode(4,15,0,0,6)
	  SLPJ A 0 A_SetScale(0.15)	 
      SLPJ B 1
	  SLPJ A 0 A_Explode(4,15,0,0,6)
	  SLPJ A 0 A_SetRoll(roll+20)
	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  Loop
   Var3:
   	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  SLPJ A 0 A_Explode(4,15,0,0,6)
      SLPJ C 1
	  SLPJ A 0 A_Explode(4,15,0,0,6)
	  SLPJ A 0 A_SetRoll(roll+20)
	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  Loop
   Var4:
   	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  SLPJ A 0 A_Explode(4,15,0,0,6)
	  SLPJ A 0 A_SetScale(0.15)
      SLPJ D 1
	  SLPJ A 0 A_Explode(4,15,0,0,6)
	  SLPJ A 0 A_SetRoll(roll+20)
	  SLPJ A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  Loop	  
   Death:
      SPRI A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
      SPRI ABCDE 1
	  Stop
   } 
}

Actor ScrapPierceDMG
{
   Speed 2
   Radius 10
   Height 5
   Damage (6)
   DamageType "ScrapLauncher"
   PROJECTILE
   +BLOODLESSIMPACT
   +NODAMAGETHRUST
   +DONTSPLASH
   +DONTBLAST
   +THRUSPECIES
   Species "Player"   
	States
	{
   Spawn:
      TNT1 A 0
	  TNT1 A 0 A_Jump(180,"PainlessSpawn")
      TNT1 A 1
      Stop
   PainlessSpawn:
      TNT1 A 0 A_ChangeFlag("PAINLESS",1)
      TNT1 A 1
      Stop	  
   Death:
      TNT1 A 0
      Stop
   }
}

Actor ScrapTrail
{
    Radius 2
    Height 2
    +NOINTERACTION
    Scale 0.01
    RenderStyle Add
    Alpha 0.4
    Translation "80:111=160:167"
    states
    {
    Spawn:
	IAGL A 1 A_FadeOut (0.1)
	Loop
    }
}

Actor ScrapTrailB
{
    Radius 2
    Height 2
    +NOINTERACTION
    Scale 0.03
    RenderStyle Add
    Alpha 0.4
    Translation "80:111=160:167"
    states
    {
    Spawn:
	IAGL A 1 A_FadeOut (0.1)
	Loop
    }
}

Actor ScrapLauncherExtraFXA
{
   Speed 10
   Radius 2
   Height 2
   Scale 0.01
   RenderStyle "AddStencil"
   StencilColor "Red"
   WeaveIndexXY 6
   WeaveIndexZ 6
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +DONTSPLASH
   ReactionTime 20
   States
   {
   Spawn:
	  NDLT A 0 NoDelay A_Jump(128,"RiseOnly")
   SpawnFall:
      NDTL A 1 A_Weave(frandom(2,5), frandom(2,5), frandom(-2,2), frandom(-2,2))
	  TNT1 A 1 ThrustThingZ(0,4,0,0)
	  NDTL A 0 A_Jump(128,2)
	  NDTL A 0 A_CountDown
	  NDTL A 0
      Loop
   RiseOnly:
	  NDTL A 1 ThrustThingZ(0,4,0,0)
	  TNT1 A 1 ThrustThing(frandom(0,359),1,0,0)
	  NDTL A 0 A_Jump(128,2)
	  NDTL A 0 A_CountDown
	  NDTL A 0
      Loop   
   Death:
      NDTL A 1
	  NDTL A 0 A_FadeOut(0.2,1)
      Loop
   }
}

Actor ScrapLauncherExtraFXB : ScrapLauncherExtraFXA
{
   RenderStyle "Stencil"
   StencilColor "Yellow"
}

Actor ScrapLauncherExtraFXC : ScrapLauncherExtraFXA
{
   RenderStyle "Stencil"
   StencilColor "Orange"
}

/*
Actor ScrapLauncherExtraFXA
{
   Speed 20
   Radius 2
   Height 2
   Scale 0.15
   Gravity 0.9
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +ROLLSPRITE
   -NOGRAVITY
   +DONTSPLASH
   ReactionTime 12
   States
   {
   Spawn:
      SLPX A 0 NoDelay A_SetSpeed(frandom(10,39))
   SpawnFall:
      SLPX A 1   
	  SLPX A 0 A_SetRoll(Roll+20)
	  SLPX A 0 A_CountDown
      Loop
   Death:
      SLPX A 1
	  TNT1 A 0 A_FadeOut(0.4,1)
      Loop
   }
}

Actor ScrapLauncherExtraFXB : ScrapLauncherExtraFXA
{
   States
   {
   Spawn:
      SLPX B 0 NoDelay A_SetSpeed(frandom(10,39))
   SpawnFall:
      SLPX B 1   
	  SLPX B 0 A_SetRoll(Roll+20)
	  SLPX B 0 A_CountDown
      Loop
   Death:
      SLPX B 1
	  TNT1 A 0 A_FadeOut(0.2,1)
      Loop
   }
}
*/

ACTOR ScrapLauncherChain : LoreShot
{
  Speed 70
  Height 14
  Radius 10
  Projectile
  +ROLLSPRITE
  +THRUSPECIES
  Damage (1)
  MaxStepHeight 4
  DamageType "ScrapLauncherChain"
  ReactionTime 8
  States
  {
  Spawn:
    SLCH A 1 NoDelay A_SpawnItemEx("ScrapLauncherChainLink",0,0,0,0,0,0,0,SXF_TRANSFERROLL)
	SLCH A 0 A_SetRoll(roll+12)
	SLCH A 0 A_CountDown
    Loop
  Death:
    SLCH A 2
    Stop
  XDeath:
    SLCH A 2 A_GiveToTarget("ScrapLauncherFirerateBonus",1)
	Stop
  }
}

ACTOR ScrapLauncherChainL2 : ScrapLauncherChain
{
  Speed 80
  Height 14
  Radius 10
  Projectile
  +ROLLSPRITE
  +THRUSPECIES
  Damage (1)
  MaxStepHeight 4
  DamageType "ScrapLauncherChain"
  ReactionTime 11
  States
  {
  Spawn:
    SLCH A 1 NoDelay A_SpawnItemEx("ScrapLauncherChainLink",0,0,0,0,0,0,0,SXF_TRANSFERROLL)
	SLCH A 0 A_SetRoll(roll+12)
	SLCH A 0 A_CountDown
    Loop
  Death:
    SLCH A 2
    Stop
  XDeath:
    SLCH A 2 A_GiveToTarget("ScrapLauncherFirerateBonus",1)
	Stop	
  }
}

Actor ScrapLauncherChainLink
{
   Speed 0
   Radius 2
   Height 2
   Scale 1
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +ROLLCENTER
   +ROLLSPRITE
   +DONTSPLASH
   ReactionTime 5
   States
   {
   Spawn:
      SLCL A 1   
	  SLCL A 0 A_CountDown
      Loop
   Death:
      SLCL A 1
      Stop
   }
}

ACTOR ScrapChainLinkProjectile //L3 Projectile
{
   Radius 10
   Height 5
   Speed 40
   DamageType "ScrapLauncher"  
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +BLOODLESSIMPACT
   +THRUSPECIES
   +ROLLSPRITE
   +CANBOUNCEWATER
   +DONTBOUNCEONSKY
   +BOUNCEONACTORS
   BounceType "Hexen"
   BounceCount 6
   BounceFactor 9.0
   WallBounceFactor 9.0   
   Species "Player"   
   Scale 0.5
   DeathSound "weapons/scraplauncherprojpool"
   ReactionTime 125
   States
   {
   Spawn:
      SLCL A 1
	  SLCL A 0 A_SetRoll(roll+20)
	  SLCL A 0 A_SpawnItemEx("ScrapPierceDMG",0,0,0,0,0,0)
	  SLCL A 0 A_Explode(4,15,0,0,6)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",0,0,2,0,0,0,0,SXF_NOCHECKPOSITION)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",(5 *momx)/-35.0,-(5 *momy)/-35.0,2+(5 *momz)/-35.0,0,0,0,0,SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",(10*momx)/-35.0,-(10*momy)/-35.0,2+(10*momz)/-35.0,0,0,0,0,SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",(15*momx)/-35.0,-(15*momy)/-35.0,2+(15*momz)/-35.0,0,0,0,0,SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",(20*momx)/-35.0,-(20*momy)/-35.0,2+(20*momz)/-35.0,0,0,0,0,SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",(25*momx)/-35.0,-(25*momy)/-35.0,2+(25*momz)/-35.0,0,0,0,0,SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION)
      SLCL A 0 A_SpawnItemEx("ScrapTrailB",(30*momx)/-35.0,-(30*momy)/-35.0,2+(30*momz)/-35.0,0,0,0,0,SXF_ABSOLUTEANGLE | SXF_NOCHECKPOSITION)	  
	  SLCL A 0 A_CountDown
      Loop	  
   Death:
      SPRI A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
      SPRI ABCDE 1
	  Stop
   } 
}

ACTOR ScrapLauncherFirerateBonus : Inventory
{
   -INVBAR
   +UNDROPPABLE
   Inventory.MaxAmount 1
   Inventory.InterHubAmount 1
   Inventory.Icon TNT1A0
}