//MECHANICS
ACTOR ShotwaveWeaponS3 : ShotwaveWeapon
{
  Weapon.SlotNumber 3
  Tag "Shotwave"
  States
  {
   Deselect:
      TNT1 AA 0 A_Lower
      TYRS A 1 A_Lower
      Loop  
   Select:
	  TYRS A 0 A_StopSound(5)
      TYRS A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:	
      TNT1 AA 0 A_Raise
      TYRS A 1 A_Raise
      Loop 
  }
}