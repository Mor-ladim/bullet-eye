//MECHANICS
ACTOR SpreadShotWeaponS2 : SpreadShotWeapon
{
   Weapon.SlotNumber 2
   Tag "Spread Shot"
   States
   {
   Deselect:
      TNT1 AA 0 A_Lower
      _WSH A 1 A_Lower
      Loop  
   Select:
	  TNT1 A 0 A_StopSound(5)
      _WSH A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:
      TNT1 AA 0 A_Raise   
      _WSH A 1 A_Raise
      Loop
   }
}