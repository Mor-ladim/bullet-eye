//MECHANICS
ACTOR VolleyShotWeaponS2 : VolleyShotWeapon
{
   Weapon.SlotNumber 2
   Tag "Volley Shot"
   States
   {
   Deselect:
      TNT1 AA 0 A_Lower
      _WVS A 1 A_Lower
      Loop  
   Select:
	  TNT1 A 0 A_StopSound(5)
      _WVS A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:
      TNT1 AA 0 A_Raise   
      _WVS A 1 A_Raise
      Loop
   }
}