//MECHANICS
ACTOR SpreadShotWeapon : BEWeaponBase
{
   Inventory.PickupSound "misc/w_pkup"
   Inventory.PickupMessage ""
   Inventory.Icon "WEAP02"
   Weapon.SlotNumber 1
   Weapon.AmmoType1 "AmmoSpreadShot"
   Weapon.AmmoType2 "AmmoAreaEnergy"
   Weapon.BobStyle Inverse
   Weapon.BobSpeed 2.3
   Weapon.BobRangeX 0.5
   Weapon.BobRangeY 0.3
   Weapon.UpSound "beplayer/weapswitchspreadshot"
   BEWeaponBase.WeaponPowerupItem "NewSpreadShotPowerup"
   BEWeaponBase.ReloadProperties "weapons/spreadshotreloadstage1", "weapons/spreadshotammo", "weapons/spreadshotreloadstage2"
   BEWeaponBase.Sprite "WEAPH02"
   BEWeaponBase.Type 1
   BEWeaponBase.Attributes 2, 3, 5, 5
   BEWeaponBase.EffectTexts "- Fires a spread of projectiles in a burst. Every time a projectile hits an enemy, it is cloned into two more shots at an angle.", "- Burst delay is decreased when closer to an enemy. An extra projectile is cloned from behind an enemy.", "- Enemies that are slain by the weapon leave behind unstable energy. The energy will fire up to 6 shots at an enemy before depleting."
   BEWeaponBase.FlavorText "The staple weapon for the Plasma Troopers mercenary squad, the Spread Shot is pretty aptly named--it fires shots in a spread. It covers a wide ground, allowing for the group to sweep areas efficiently in tandem. You're just by yourself, but it should still do well."
   +WEAPON.ALT_AMMO_OPTIONAL 
   +WEAPON.NO_AUTO_SWITCH
   +WEAPON.CHEATNOTWEAPON
   Tag "Spread Shot"   
   States
   {
   Spawn:
      SPPP A -1
      Loop
   Ready:
      _WSH A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4)
      Loop
	  _WSH A 1 Offset( 0, 80) //Head here after Sub-Weapon usage.
	  _WSH A 1 Offset( 0, 60)
	  _WSH A 1 Offset( 0, 43)
	  TNT1 A 0 A_WeaponReady(WRF_NOFIRE|WRF_NOSWITCH)
	  Goto Ready 
   Deselect:
      TNT1 AA 0 A_Lower
      _WSH A 1 A_Lower
      Loop  
   Select:
	  _WSH A 0 A_StopSound(5)
      _WSH A 0 A_SelectWeapon("IronAnnihilator")
   SelectFall:
      TNT1 AA 0 A_Raise   
      _WSH A 1 A_Raise
      Loop 
  Fire:
      _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_JumpIf(A_GetWeaponLevel() >= 2,"FireL2")
	  _WSH B 1 Bright A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_AlertMonsters
	  _WSH A 0 A_PlaySound("weapons/spreadshotfire",CHAN_WEAPON,1,0)
	  _WSH F 1 A_FireBEProjectile("SpreadShotProj",0,1,0,0,0,0)
	  _WSH A 0 A_GunFlash
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_TakeDurability(3)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 2
	  _WSH G 1 A_FireBEProjectile("SpreadShotProj",-3,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",3,1,-2,0,0,0)
	  _WSH A 0 A_GunFlash
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 2
	  _WSH H 1 A_FireBEProjectile("SpreadShotProj",0,1,0,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",-6,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",6,1,-2,0,0,0)  
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_GunFlash
	  _WSH A 2
	  _WSH G 1 A_FireBEProjectile("SpreadShotProj",-3,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",3,1,-2,0,0,0) 
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",-6,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",6,1,-2,0,0,0) 	  
	  _WSH A 0 A_TakeAmmo(2)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_GunFlash
	  _WSH A 2
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH I 1 Offset( 0, 36)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH J 1 Offset( 0, 42)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH H 1 Offset( 0, 40)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH G 1 Offset( 0, 36)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH F 1 Offset( 0, 34)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 14 Offset( 0, 32) //THE DEFAULT POSITION
	  _WSH A 0 A_ReFire
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_PlaySound("weapons/spreadshotclick",CHAN_AUTO,0.8,0)
	  _WSH A 0 A_JumpIfInventory("PowerDoubleFiringSpeed",1,4) //Otherwise it won't be affected by faster firing powerups.
	  _WSH CDE 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH DC 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 4
      Goto Ready
  FireL2:
      _WSH A 0 A_CheckWeaponFire
	  _WSH B 1 Bright A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_CheckProximity("FireFaster","BEEnemyBase",200,1,CPXF_ANCESTOR) 
	  _WSH A 0 A_AlertMonsters
	  _WSH A 0 A_PlaySound("weapons/spreadshotfire",CHAN_WEAPON,1,0)
	  _WSH F 1 A_FireBEProjectile("SpreadShotProj",0,1,0,0,0,0)
	  _WSH A 0 A_GunFlash
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_TakeDurability(3)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 2
	  _WSH G 1 A_FireBEProjectile("SpreadShotProj",-3,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",3,1,-2,0,0,0)
	  _WSH A 0 A_GunFlash
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 2
	  _WSH H 1 A_FireBEProjectile("SpreadShotProj",0,1,0,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",-6,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",6,1,-2,0,0,0)  
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_GunFlash
	  _WSH A 2
	  _WSH G 1 A_FireBEProjectile("SpreadShotProj",-3,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",3,1,-2,0,0,0) 
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",-6,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",6,1,-2,0,0,0) 	  
	  _WSH A 0 A_TakeAmmo(2)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_GunFlash
	  _WSH A 2
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH I 1 Offset( 0, 36)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH J 1 Offset( 0, 42)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH H 1 Offset( 0, 40)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH G 1 Offset( 0, 36)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH F 1 Offset( 0, 34)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 14 Offset( 0, 32) //THE DEFAULT POSITION
	  _WSH A 0 A_ReFire
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_PlaySound("weapons/spreadshotclick",CHAN_AUTO,0.8,0)
	  _WSH A 0 A_JumpIfInventory("PowerDoubleFiringSpeed",1,4) //Otherwise it won't be affected by faster firing powerups.
	  _WSH CDE 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH DC 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 4
      Goto Ready
  FireFaster:
      _WSH A 0 A_CheckWeaponFire
	  _WSH B 1 Bright A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_AlertMonsters
	  _WSH A 0 A_PlaySound("weapons/spreadshotfire",CHAN_WEAPON,1,0)
	  _WSH F 1 A_FireBEProjectile("SpreadShotProj",0,1,0,0,0,0)
	  _WSH A 0 A_GunFlash
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_TakeDurability(3)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 2
	  _WSH G 1 A_FireBEProjectile("SpreadShotProj",-3,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",3,1,-2,0,0,0)
	  _WSH A 0 A_GunFlash
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 2
	  _WSH H 1 A_FireBEProjectile("SpreadShotProj",0,1,0,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",-6,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",6,1,-2,0,0,0)  
	  _WSH A 0 A_TakeAmmo(1)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_GunFlash
	  _WSH A 2
	  _WSH G 1 A_FireBEProjectile("SpreadShotProj",-3,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",3,1,-2,0,0,0) 
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",-6,1,2,0,0,0)
	  _WSH A 0 A_FireBEProjectile("SpreadShotProj",6,1,-2,0,0,0) 	  
	  _WSH A 0 A_TakeAmmo(2)
	  _WSH A 0 A_CheckWeaponFire
	  _WSH A 0 A_GunFlash
	  _WSH A 2
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH I 1 Offset( 0, 36)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH J 1 Offset( 0, 42)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH H 1 Offset( 0, 40)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH G 1 Offset( 0, 36)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH F 1 Offset( 0, 34)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 7 Offset( 0, 32) //THE DEFAULT POSITION
	  _WSH A 0 A_ReFire
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_PlaySound("weapons/spreadshotclick",CHAN_AUTO,0.8,0)
	  _WSH A 0 A_JumpIfInventory("PowerDoubleFiringSpeed",1,4) //Otherwise it won't be affected by faster firing powerups.
	  _WSH CDE 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH DC 1 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 0 A_WeaponReady(WRF_NOPRIMARY|WRF_ALLOWRELOAD|WRF_ALLOWZOOM|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4|WRF_NOBOB)
	  _WSH A 4
      Goto Ready
   Flash:
      TNT1 A 1 Bright A_Light1
	  TNT1 A 1 Bright A_Light0
      Stop
   }
}

ACTOR SpreadShotProj : BEPlayerProjectile
{
   Speed 55
   Radius 4
   Height 4
   Damage (3*random(3,5))
   DamageType "SpreadShot"
   MissileHeight 8
   MissileType "SpreadShotTrail"    
   Scale 1
   PROJECTILE
   +BRIGHT
   +THRUSPECIES
   Species "Player"   
   RenderStyle Add
   ReactionTime 10
   States
   {
   Spawn: 
	  SPRS A 1 NoDelay A_JumpIfInventory("BonusSpreadShotL1",1,"SpawnLongShotBonus",AAPTR_MASTER)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 0 A_CountDown
	  Loop
   SpawnLongShotBonus: 
	  SPRS A 1
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 1
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)	  
	  SPRS A 0 A_CountDown
	  Loop	  
   Death:
	  SPRS A 0 A_CheckProximity("DeathLowFX", "SpreadShotProjImpactFX", 250, 5)
	  SPRS A 0 A_CheckProximity("DeathLowFX", "SpreadShotGlowImpact", 250, 5)   
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjImpactFX", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 0 A_SpawnItemEx("SpreadShotGlowImpact", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPPJ A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SSIM ABCD 2
      Stop
   DeathLowFX:
	  SPPJ A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SSIM ABCD 2
      Stop
   XDeath:
      SPRS A 0 A_JumpIf(GetTargetWeaponLevel("SpreadShotWeapon") >= 2,"XDeathL2")
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,35,0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,-35,0)
	  SPRS A 0 A_CheckProximity("XDeathLowFX", "SpreadShotProjImpactFX", 250, 5)
	  SPRS A 0 A_CheckProximity("XDeathLowFX", "SpreadShotGlowImpact", 250, 5)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjImpactFX", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 0 A_SpawnItemEx("SpreadShotGlowImpact", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPPJ A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SPPJ A 0 A_PlaySound("weapons/spreadshotimpact",6,0.3,0)
	  SSIM ABCD 2	  
	  Stop
   XDeathLowFX:
      SPRS A 0 A_JumpIf(GetTargetWeaponLevel("SpreadShotWeapon") >= 2,"XDeathL2")
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,35,0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,-35,0)
	  SPPJ A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SPPJ A 0 A_PlaySound("weapons/spreadshotimpact",6,0.3,0)
	  SSIM ABCD 2	  
	  Stop	  
   XDeathL2:
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,0,0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,35,0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,-35,0)
	  SPRS A 0 A_CheckProximity("XDeathL2LowFX", "SpreadShotProjImpactFX", 250, 5)
	  SPRS A 0 A_CheckProximity("XDeathL2LowFX", "SpreadShotGlowImpact", 250, 5)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjImpactFX", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 0 A_SpawnItemEx("SpreadShotGlowImpact", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPPJ A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SPPJ A 0 A_PlaySound("weapons/spreadshotimpact",6,0.3,0)
	  SSIM ABCD 2	  
	  Stop
   XDeathL2LowFX:
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,0,0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,35,0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjHitDelay",cos(pitch)*1,0,0-(sin(pitch)*1),cos(pitch)*55,0,-sin(pitch)*55,-35,0)
	  SPPJ A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	  SPPJ A 0 A_PlaySound("weapons/spreadshotimpact",6,0.3,0)
	  SSIM ABCD 2	  
	  Stop	  
   }
}

ACTOR SpreadShotProjBonus : SpreadShotProj
{
	Damage (6*random(3,5))
}

ACTOR SpreadShotProjHitDelay : SpreadShotProj
{
   MissileHeight 8
   MissileType "DoomBuilderCamera"
   +THRUACTORS
   States
   {
   Spawn: 
	  SPRS A 4
	  SPRS A 0 A_ChangeFlag("THRUACTORS",0)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 0 A_CountDown
   SpawnFall:
   	  SPRS A 1 A_JumpIfInventory("BonusSpreadShotL1",1,"SpawnFallLongShot",AAPTR_MASTER)
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPRS A 0 A_CountDown
	  Loop
   SpawnFallLongShot:
   	  SPRS A 1
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)
   	  SPRS A 1
	  SPRS A 0 A_SpawnItemEx("SpreadShotProjGlow", 0, 0, 0, 0, 0, 0, 0, 32)	  
	  SPRS A 0 A_CountDown
	  Loop	  
   }
}

ACTOR SpreadShotUnstableEnergy
{
	 Radius 10
	 Height 10
	 Mass 10
	 Scale 0.15
	 Alpha 0.45
	 Health 200
	 Speed 0
	 MaxTargetRange 600
	 Args 100,6
	 RenderStyle Add
	 MONSTER
	 +BRIGHT
	 +NOBLOOD  
	 +NOICEDEATH
	 -SHOOTABLE
	 +MISSILEEVENMORE
	 +QUICKTORETALIATE
	 +CANTSEEK
	 +DONTTHRUST
	 +NODAMAGE
	 +DONTBLAST
	 +NOGRAVITY
	 +THRUACTORS
	 States
	 {
	 Spawn:
		SHGL A 1 NoDelay A_ChangeFlag("FRIENDLY",1)
		SHGL A 0 A_RearrangePointers(AAPTR_DEFAULT, AAPTR_TARGET, AAPTR_DEFAULT)
		SHGL A 0 A_JumpIfInventory("BonusSpreadShotL31",1,"EquipBonusSetup",AAPTR_MASTER)
	 SpawnFall:
	    SHGL A 0 A_Look
		SHGL A 1 A_SetScale(0.1)
		SHGL A 1 A_SetScale(0.15)
		Loop
	 EquipBonusSetup:
	    SHGL A 0 A_SetArg(1,15)
		SHGL A 0 A_SetArg(0,200)
		Goto SpawnFall
	 See:
		SHGL A 0 A_Chase
		SHGL A 1 A_SetScale(0.1)
		SHGL A 1 A_SetScale(0.15)		
		SHGL A 0 A_CountdownArg(0,"Death")
		Loop
	 Missile:
	    SHGL A 0 A_JumpIfInventory("BonusSpreadShotL32",1,"MissileEquipBonusSetup",AAPTR_MASTER)
		SHGL A 1
		SHGL A 0 A_FaceTarget
		SHGL A 0 A_PlaySound("equipment/sentrysguardturretfire",CHAN_AUTO,1,0)
		SHGL A 1 A_SpawnProjectile("SpreadShotProj",0,0,0,0,0)
		SHGL A 0 A_CountdownArg(1,"Death")
		SHGL A 12
		Goto See
	 MissileEquipBonusSetup:
	    SHGL A 0 A_CheckProximity("MissileEquipBonus", "BEPlayerBase", 250, 1, CPXF_ANCESTOR|CPXF_CHECKSIGHT)
		Goto Missile+1
	 MissileEquipBonus:		
		SHGL A 1
		SHGL A 0 A_FaceTarget
		SHGL A 0 A_PlaySound("equipment/sentrysguardturretfire",CHAN_AUTO,1,0)
		SHGL A 0 A_SpawnItemEx("RapidVulcanCriticalPuff",0,0,0,0,0,0,SXF_NOCHECKPOSITION)
		SHGL A 1 A_SpawnProjectile("SpreadShotProjBonus",0,0,0,0,0)
		SHGL A 0 A_CountdownArg(1,"Death")
		SHGL A 12
		Goto See	  
	 Pain:
		SHGL A 1
		Goto See
	 Death:
		SHGL A 1 A_SetScale(scalex+0.1)
		SHGL A 0 A_FadeOut(0.15)
		Loop
	 }
}

ACTOR SpreadShotTrail
{
	Radius 2
	Height 2
	Renderstyle Add
	Alpha 0.2
	Scale 0.08
	+NOINTERACTION
	+THRUACTORS
	+BRIGHT
    States
    {
    Spawn:
	   SHGL A 1
	   SHGL A 0 A_FadeOut (0.05)
	   SHGL A 1
	   SHGL A 0 A_FadeOut (0.05)
	   SHGL A 1
	   SHGL A 0 A_FadeOut (0.05)
	   SHGL A 1
	SpawnFall:
	   SHGL A 0 A_FadeOut (0.1)
	   SHGL A 1
	   Loop
    }
}

ACTOR SpreadShotGlowImpact
{
   Radius 2
   Height 2
   RenderStyle Add
   Alpha 0.6
   +BRIGHT
   +THRUACTORS
   +DONTSPLASH
   +NOGRAVITY
   Scale 0.2
   States
   {
   Spawn:
	  SHGL A 1 NoDelay A_SetScale(scalex+0.1)
	  SHGL A 0 A_FadeOut(0.1)
      Loop
   Death:
      SHGL A 1
      Stop	  
   } 
}

Actor SpreadShotSpark
{
   Speed 20
   Radius 4
   Height 4
   Scale 0.2
   Gravity 1.2
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   -NOGRAVITY
   +DONTSPLASH
   RenderStyle AddStencil
   StencilColor "Green"
   ReactionTime 6
   States
   {
   Spawn:
      SPTL A 3
	  SPTL A 0 A_SpawnItemEx("SpreadShotSparkGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPTL A 3
	  SPTL A 0 A_SpawnItemEx("SpreadShotSparkGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPTL A 3
	  SPTL A 0 A_SpawnItemEx("SpreadShotSparkGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPTL A 3
	  SPTL A 0 A_SpawnItemEx("SpreadShotSparkGlow", 0, 0, 0, 0, 0, 0, 0, 32)
	  SPTL A 0 A_CountDown
      Loop
   Death:
      TNT1 A 1
	  TNT1 A 0 A_FadeOut(0.15,1)
      Loop
   }
}

Actor SpreadShotSparkSlow : SpreadShotSpark
{
   Speed 10
}

Actor SpreadShotSparkFast : SpreadShotSpark
{
   Speed 40
}

Actor SpreadShotSparkGlow
{
   +NOINTERACTION
   +THRUACTORS
   RenderStyle Stencil
   StencilColor "Green"
   Alpha 0.8
   Scale 0.2
   States
   {
   Spawn:
	  SPGL A 1 Bright
      Stop
   }
}

Actor SpreadShotProjGlow
{
   Radius 2
   Height 2
   +THRUACTORS
   +NOINTERACTION
   +ROLLSPRITE
   RenderStyle Add
   Alpha 0.4
   Scale 0.15
   States
   {
   Spawn:
	  BPRX A 1 Bright
	  BPRX A 0 A_SetRoll(frandom(0,359),SPF_INTERPOLATE,0)
	  BPRX A 0 A_SpawnItemEx("SpreadShotProjShockTrail", 0, 0, 0, 0, 0, 0, 0, 32)
	  BPRX A 1 Bright
	  BPRX A 0 A_SetRoll(frandom(0,359),SPF_INTERPOLATE,0)
	  BPRX A 1 Bright
	  BPRX A 0 A_SetRoll(frandom(0,359),SPF_INTERPOLATE,0)
	  BPRX A 1 Bright
	  BPRX A 0 A_SetRoll(frandom(0,359),SPF_INTERPOLATE,0)	  
      Stop
   Death:
      BPRX A 2
	  BPRX A 0 A_FadeOut(0.2)
	  Loop
   }
}

Actor SpreadShotProjShockTrail
{
   Radius 2
   Height 2
   +THRUACTORS
   +NOINTERACTION
   RenderStyle Add
   Alpha 0.1
   Scale 0.12
   States
   {
   Spawn:
	  BPRX B 6 Bright
      Stop
   Death:
      BPRX B 2
	  BPRX B 0 A_FadeOut(0.2)
	  Loop
   }
}

ACTOR SpreadShotProjImpactFX
{
    +NOINTERACTION
	+THRUACTORS
	+ROLLSPRITE
    RenderStyle Add
    Alpha 0.4
    Scale 0.8
	+BRIGHT
    States
{
   Spawn:
    TTRL A 0 NoDelay A_SetRoll(frandom(0,359),0,0)
	TTRL A 0 A_SpawnItemEx("SpreadShotProjImpactFX2", 0, 0, 0, 0, 0, 0, 0, 32)
	TTRL A 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
	TTRL B 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)
 	TTRL C 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
	TTRL D 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
   SpawnFall:	
	TTRL A 0 A_SetScale(scalex+0.3)
 	TTRL D 1
	TTRL A 0 A_FadeOut(0.1)
	Loop
   Death:
    TTRL A 0
	Stop
   }
}

ACTOR SpreadShotProjImpactFX2
{
    +NOINTERACTION
	+THRUACTORS
	+ROLLSPRITE
    RenderStyle Add
    Alpha 0.4
    Scale 1.4
	+BRIGHT
    States
{
   Spawn:
    TTRL A 0 NoDelay A_SetRoll(frandom(0,359),0,0)
	TTRL A 0 A_SpawnItemEx("SpreadShotProjImpactFX3", 0, 0, 0, 0, 0, 0, 0, 32)
	TTRL A 0 A_SpawnItemEx("SpreadShotImpactSweetener", 0, 0, 0, 0, 0, 0, 0, 32)
	TTRL A 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
	TTRL B 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)
 	TTRL C 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
	TTRL D 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
   SpawnFall:	
	TTRL A 0 A_SetScale(scalex+0.3)
 	TTRL D 1
	TTRL A 0 A_FadeOut(0.1)
	Loop
   Death:
    TTRL A 0
	Stop
   }
}

ACTOR SpreadShotProjImpactFX3
{
    +NOINTERACTION
	+THRUACTORS
	+ROLLSPRITE
    RenderStyle Stencil
	StencilColor "Green"
    Alpha 0.2
    Scale 1.8
	+BRIGHT
    States
{
   Spawn:
    TTRL A 0 NoDelay A_SetRoll(frandom(0,359),0,0)
	TTRL A 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
	TTRL B 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)
 	TTRL C 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
	TTRL D 1
	TTRL A 0 A_SetScale(scalex+0.15)
	TTRL A 0 A_FadeOut(0.05)	
   SpawnFall:	
	TTRL A 0 A_SetScale(scalex+0.3)
 	TTRL D 1
	TTRL A 0 A_FadeOut(0.1)
	Loop
   Death:
    TTRL A 0
	Stop
   }
}

ACTOR SpreadShotImpactSweetener
{ 
    Scale 0.45
    +NOBLOCKMAP
    +NOGRAVITY
    +THRUACTORS
	+NOINTERACTION
	+ROLLSPRITE
    Renderstyle Stencil
	StencilColor "Green"
    Alpha 0.3
    States
    {
    Spawn:
	  RVCT A 0 NoDelay A_SetRoll(roll+random(0,359))
      RVCT ABCD 1 BRIGHT
      Stop
    }
}
