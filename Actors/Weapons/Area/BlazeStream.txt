//MECHANICS
Actor BlazeStreamMainFlame //Visual. Passes through enemies and deals no damage.
{
	Radius 2
	Height 2
	Speed 25
	Scale 0.2
	RenderStyle Add
	Alpha 0.6
	PROJECTILE
	+THRUACTORS
	+RANDOMIZE
	+NOBLOCKMAP
	+BRIGHT
    +THRUSPECIES
    Species "Player"	
	States
	{
	Spawn:
	    BSF3 A 0 NoDelay A_Jump(128,"Var2")
		BSF3 A 1
		BSF3 A 0 A_Explode(3,5,0,1,5)		
		BSF3 A 1
		BSF3 A 0 A_Jump(20,"DeathEarly")
		BSF3 A 0 A_Explode(3,5,0,1,5)
		BSF3 A 0 A_SpawnItemEx("FlameSegmentDMG",0,0,0)
		BSF3 A 0 A_SetScale(0.6)
		BSF3 B 2
		BSF3 A 0 A_Jump(40,"DeathEarly")
		BSF3 A 0 A_Explode(3,5,0,1,5)
		BSF3 C 1
		BSF3 A 0 A_Jump(80,"DeathEarly")
		BSF3 A 0 A_SetScale(0.8)
		BSF3 A 0 A_SpawnItemEx("FlameSegmentDMG",0,0,0)
		BSF3 A 0 A_Weave(4,4,frandom(-5,5),frandom(-5,5))
		BSF3 A 1 
		BSF3 A 0 A_SetScale(1.0)
		BSF3 A 0 A_SetTranslucent(0.5)
		BSF3 B 1 A_SetTranslucent(0.4)
		BSF3 A 0 A_Jump(100,"DeathEarly")
		BSF3 A 0 A_SetScale(1.2)
		BSF3 A 0 A_SpawnItemEx("FlameSegmentDMG",0,0,0)
		BSF3 C 1 A_SetTranslucent(0.3)
		BSF3 A 0 A_SetScale(1.4)
		BSF3 A 1
		BSF3 A 0 A_Explode(2,5,0,1,5)
		Stop
	Var2:
		BSF4 A 1
		BSF4 A 0 A_Explode(3,5,0,1,5)		
		BSF4 A 1
		BSF4 A 0 A_Jump(20,"DeathEarly")
		BSF4 A 0 A_Explode(3,5,0,1,5)
		BSF4 A 0 A_SpawnItemEx("FlameSegmentDMG",0,0,0)
		BSF3 A 0 A_SetScale(0.6)
		BSF4 B 2
		BSF4 A 0 A_Jump(40,"DeathEarly")
		BSF4 A 0 A_Explode(3,5,0,1,5)
		BSF4 C 1
		BSF4 A 0 A_Jump(80,"DeathEarly")
		BSF4 A 0 A_SetScale(0.8)
		BSF4 A 0 A_SpawnItemEx("FlameSegmentDMG",0,0,0)
		BSF3 A 0 A_Weave(4,4,frandom(-5,5),frandom(-5,5))
		BSF4 A 1 
		BSF4 A 0 A_SetScale(1.0)
		BSF4 A 0 A_SetTranslucent(0.5)
		BSF4 B 1 A_SetTranslucent(0.4)
		BSF4 A 0 A_Jump(100,"DeathEarly")
		BSF4 A 0 A_SetScale(1.2)
		BSF4 A 0 A_SpawnItemEx("FlameSegmentDMG",0,0,0)
		BSF4 C 1 A_SetTranslucent(0.3)
		BSF4 A 0 A_SetScale(1.4)
		BSF4 A 1
		BSF3 A 0 A_Explode(2,5,0,1,5)
		Stop		
	Death:
	    FVIP A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	    FVIP FGHI 1 A_SetScale(0.8)
		Stop
	DeathEarly:
	    FVIP FGHI 1 A_SetScale(0.45)
		Stop		
	}
}

Actor BlazeStreamMainFlameX //Visual. Passes through enemies and deals no damage.
{
	Radius 2
	Height 2
	Speed 25
	Scale 0.2
	RenderStyle Add
	Alpha 0.6
	PROJECTILE
	+THRUACTORS
	+RANDOMIZE
	+NOBLOCKMAP
	+BRIGHT
    +THRUSPECIES
    Species "Player"	
	States
	{
	Spawn:
	    BSF3 A 0 NoDelay A_Jump(128,"Var2")
		BSF3 A 1
		BSF3 A 0 A_Explode(3,5,0,1,5)
		BSF3 A 1
		BSF3 A 0 A_Jump(20,"DeathEarly")
		BSF3 A 0 A_Explode(3,5,0,1,5)
		BSF3 A 0 A_SpawnItemEx("FlameSegmentDMGSlowBurn",0,0,0)
		BSF3 A 0 A_SetScale(0.6)
		BSF3 B 2
		BSF3 A 0 A_Jump(40,"DeathEarly")
		BSF3 A 0 A_Explode(3,5,0,1,5)
		BSF3 C 1
		BSF3 A 0 A_Jump(80,"DeathEarly")
		BSF3 A 0 A_SetScale(0.8)
		BSF3 A 0 A_SpawnItemEx("FlameSegmentDMGSlowBurn",0,0,0)
		BSF3 A 0 A_Weave(4,4,frandom(-5,5),frandom(-5,5))
		BSF3 A 1 
		BSF3 A 0 A_SetScale(1.0)
		BSF3 A 0 A_SetTranslucent(0.5)
		BSF3 B 1 A_SetTranslucent(0.4)
		BSF3 A 0 A_Jump(100,"DeathEarly")
		BSF3 A 0 A_SetScale(1.2)
		BSF3 A 0 A_SpawnItemEx("FlameSegmentDMGSlowBurn",0,0,0)
		BSF3 C 1 A_SetTranslucent(0.3)
		BSF3 A 0 A_SetScale(1.4)
		BSF3 A 1
		BSF3 A 0 A_Explode(2,5,0,1,5)
		Stop
	Var2:
		BSF4 A 1
		BSF4 A 0 A_Explode(3,5,0,1,5)		
		BSF4 A 1
		BSF4 A 0 A_Jump(20,"DeathEarly")
		BSF4 A 0 A_Explode(3,5,0,1,5)
		BSF4 A 0 A_SpawnItemEx("FlameSegmentDMGSlowBurn",0,0,0)
		BSF3 A 0 A_SetScale(0.6)
		BSF4 B 2
		BSF4 A 0 A_Jump(40,"DeathEarly")
		BSF4 A 0 A_Explode(3,5,0,1,5)
		BSF4 C 1
		BSF4 A 0 A_Jump(80,"DeathEarly")
		BSF4 A 0 A_SetScale(0.8)
		BSF4 A 0 A_SpawnItemEx("FlameSegmentDMGSlowBurn",0,0,0)
		BSF3 A 0 A_Weave(4,4,frandom(-5,5),frandom(-5,5))
		BSF4 A 1 
		BSF4 A 0 A_SetScale(1.0)
		BSF4 A 0 A_SetTranslucent(0.5)
		BSF4 B 1 A_SetTranslucent(0.4)
		BSF4 A 0 A_Jump(100,"DeathEarly")
		BSF4 A 0 A_SetScale(1.2)
		BSF4 A 0 A_SpawnItemEx("FlameSegmentDMGSlowBurn",0,0,0)
		BSF4 C 1 A_SetTranslucent(0.3)
		BSF4 A 0 A_SetScale(1.4)
		BSF4 A 1
		BSF3 A 0 A_Explode(2,5,0,1,5)
		Stop		
	Death:
	    FVIP A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
	    FVIP FGHI 1 A_SetScale(0.8)
		Stop
	DeathEarly:
	    FVIP FGHI 1 A_SetScale(0.45)
		Stop		
	}
}

Actor BlazeStreamMainFlameFX //Visual. Passes through enemies and deals no damage.
{
	Radius 2
	Height 2
	Speed 25
	Scale 0.2
	RenderStyle AddStencil
	StencilColor "Red"
	Alpha 0.06
	PROJECTILE
	+THRUACTORS
	+RANDOMIZE
	+NOBLOCKMAP
	+SPRITEFLIP
	+BRIGHT
    +THRUSPECIES
    Species "Player"	
	States
	{
	Spawn:
	    BSF3 A 0 NoDelay A_Jump(128,"Var2")
		BSF3 A 1	
		BSF3 A 1
		BSF3 A 0 A_Jump(20,"DeathEarly")
		BSF3 A 0 A_SetScale(0.6)
		BSF3 B 2
		BSF3 A 0 A_Jump(40,"DeathEarly")
		BSF3 C 1
		BSF3 A 0 A_Jump(80,"DeathEarly")
		BSF3 A 0 A_SetScale(0.8)
		BSF3 A 0 A_Weave(4,4,frandom(-5,5),frandom(-5,5))
		BSF3 A 1 
		BSF3 A 0 A_SetScale(1.0)
		BSF3 B 1
		BSF3 A 0 A_Jump(100,"DeathEarly")
		BSF3 A 0 A_SetScale(1.2)
		BSF3 C 1
		BSF3 A 0 A_SetScale(1.4)
		BSF3 A 1
		Stop
	Var2:
		BSF4 A 1		
		BSF4 A 1
		BSF4 A 0 A_Jump(20,"DeathEarly")
		BSF3 A 0 A_SetScale(0.6)
		BSF4 B 2
		BSF4 A 0 A_Jump(40,"DeathEarly")
		BSF4 C 1
		BSF4 A 0 A_Jump(80,"DeathEarly")
		BSF4 A 0 A_SetScale(0.8)
		BSF3 A 0 A_Weave(4,4,frandom(-5,5),frandom(-5,5))
		BSF4 A 1 
		BSF4 A 0 A_SetScale(1.0)
		BSF4 B 1
		BSF4 A 0 A_Jump(100,"DeathEarly")
		BSF4 A 0 A_SetScale(1.2)
		BSF4 C 1
		BSF4 A 0 A_SetScale(1.4)
		BSF4 A 1
		Stop		
	Death:
	    FVIP A 0
	    FVIP FGHI 1 A_SetScale(0.8)
		Stop
	DeathEarly:
	    FVIP FGHI 1 A_SetScale(0.45)
		Stop		
	}
}

ACTOR BlazeStreamStopFX
{
   Speed 2
   Radius 2
   Height 2
   Scale 0.3
   RenderStyle Add
   Alpha 0.6
   PROJECTILE
   +BRIGHT
   +NOGRAVITY
   +THRUACTORS
   +NOINTERACTION
   States
   {
   Spawn:
      FVIP FGHI 1  
	  Stop
   Death:
      FVIP A 0
      Stop
   }
}

Actor BlazeStreamMainFlameFX2 : BlazeStreamMainFlameFX //Visual. Passes through enemies and deals no damage.
{
	StencilColor "Yellow"
}

Actor FlameSegmentDMG //Invisible, but responsible for damage dealt by the BlazeStreamMainFlame actor.
{
   Speed 2
   Radius 10
   Height 5
   Damage (2)
   DamageType "BlazeStream"
   PROJECTILE
   +BLOODLESSIMPACT
   +NODAMAGETHRUST
   +DONTSPLASH
   +DONTBLAST
   +THRUSPECIES
   Species "Player"   
	States
	{
   Spawn:
      TNT1 A 0
	  TNT1 A 0 A_Jump(180,"PainlessSpawn")
      TNT1 A 1
      Stop
   PainlessSpawn:
      TNT1 A 0 A_ChangeFlag("PAINLESS",1)
      TNT1 A 1
      Stop	  
   Death:
      FVIP A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
      TNT1 A 0
      Stop
   }
}

Actor FlameSegmentDMGSlowBurn //Invisible, but responsible for damage dealt by the BlazeStreamMainFlame actor.
{
   Speed 2
   Radius 10
   Height 5
   Damage (2)
   PoisonDamage 15,12,105
   PoisonDamageType "BlazeStreamSlowBurn"
   DamageType "BlazeStream"
   PROJECTILE
   +BLOODLESSIMPACT
   +NODAMAGETHRUST
   +DONTSPLASH
   +DONTBLAST
   +THRUSPECIES
   +FORCEPAIN
   Species "Player"   
	States
	{
   Spawn:
      TNT1 A 0
      TNT1 A 1
      Stop
   PainlessSpawn:
      TNT1 A 0 A_ChangeFlag("PAINLESS",1)
      TNT1 A 1
      Stop	  
   Death:
      FVIP A 0 A_CustomBulletAttack(0,0,1,0,"BulletEyeActPuff",0,CBAF_AIMFACING)
      TNT1 A 1
      Stop
   }
}

ACTOR BlazeStreamRotateFlameFX //Deals no damage.
{
   Radius 2
   Height 2
   Speed 15
   Scale 0.3
   RenderStyle Add
   Alpha 0.02
   +BRIGHT
   +THRUACTORS
   +DONTBLAST
   +NOGRAVITY
   +NOINTERACTION
   +ROLLSPRITE
   ReactionTime 15
   States
   {
   Spawn:
      BSFX A 0 NoDelay A_SetRoll(roll+frandom(0,359),0,0)
   SpawnFall:
      BSFX A 1
	  BSFX A 0 A_SetRoll(roll+30,0,0)
	  BSFX A 0 A_Countdown
	  Loop
   Death:
      BSFX A 1
	  BSFX A 0 A_FadeOut(0.01)
      Loop
   } 
}

ACTOR BlazeStreamRotateFlameFXRed : BlazeStreamRotateFlameFX
{
   RenderStyle Stencil
   StencilColor "Red"
   Scale 0.2
}

ACTOR BlazeStreamRotateFlameFXOrange : BlazeStreamRotateFlameFX
{
   RenderStyle Stencil
   StencilColor "Orange"
   Scale 0.1
   Alpha 0.01
}

Actor FlameSegmentDMGPainless : FlameSegmentDMG //PAINLESS to prevent total stunlocks.
{
   +PAINLESS
}

ACTOR FireGuardFlames : FastProjectile
{
   Radius 3
   Height 3
   Speed 25
   Scale 0.5
   RenderStyle Add
   Alpha 0.6
   MissileType "FireGuardFlamesFX"
   MissileHeight 8
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +DONTBLAST
   +THRUSPECIES
   Species "Player"
   DamageType "BlazeStream"
   States
   {
   Spawn:
      BSF2 A 2
	  //BSF2 A 0 A_Explode(10,8,0,1,8)
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  BSF2 B 2
	  //BSF2 A 0 A_Explode(10,8,0,1,8)
      BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)	  
	  BSF2 C 2
	  //BSF2 A 0 A_Explode(10,8,0,1,8)
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  BSF2 A 0 A_SetScale(0.6)
	  BSF2 D 2
	  //BSF2 A 0 A_Explode(10,8,0,1,8)
	  BSF2 A 0 A_SetScale(0.7)
	  BSF2 A 2
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  //BSF2 A 0 A_Explode(10,8,0,1,8)
	  BSF2 A 0 A_SetScale(0.8)
	  BSF2 B 2
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  //BSF2 A 0 A_Explode(5,8,0,1,8)
	  BSF2 B 2
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  //BSF2 A 0 A_Explode(5,8,0,1,8)
	  BSF2 B 2
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  //BSF2 A 0 A_Explode(5,8,0,1,8)
	  BSF2 B 2
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  //BSF2 A 0 A_Explode(5,8,0,1,8)
	  BSF2 B 2
	  BSF2 A 0 A_SpawnItemEx("FireGuardDMG",0,0,5,0,0,0)
	  //BSF2 A 0 A_Explode(5,8,0,1,8)	  
	  Stop
   Death:
	  BSF2 A 1 A_FadeOut(0.15)
      Loop
   } 
}

ACTOR FireGuardFlamesSmall : FastProjectile
{
   Radius 3
   Height 3
   Speed 25
   Scale 0.2
   RenderStyle Add
   Alpha 0.03
   MissileType "FireGuardFlamesFXSmall"
   MissileHeight 8
   PROJECTILE
   +BRIGHT
   +THRUACTORS
   +DONTBLAST
   +THRUSPECIES
   Species "Player"
   DamageType "BlazeStream"
   States
   {
   Spawn:
      BSF2 ABC 1

	  BSF2 A 0 A_SetScale(0.25)
	  BSF2 D 1
	  BSF2 A 0 A_SetScale(0.3)
	  BSF2 A 1
	  BSF2 A 0 A_SetScale(0.35)
	  BSF2 BBBBB 1 
	  Stop
   Death:
	  BSF2 A 1 A_FadeOut(0.01)
      Loop
   } 
}

ACTOR FireGuardFlamesSweetener : FireGuardFlamesSmall
{
   Scale 0.05
   Alpha 0.005
}

Actor FireGuardDMG : FlameSegmentDMG
{
   Radius 15
   Height 10
   Damage (12)
}

ACTOR FireGuardFlamesFX //Deals no damage.
{
   Radius 3
   Height 3
   Speed 0
   Scale 0.5
   RenderStyle Add
   Alpha 0.45
   +BRIGHT
   +THRUACTORS
   +DONTBLAST
   +NOGRAVITY
   +NOINTERACTION
   States
   {
   Spawn:
      BSF2 A 2
	  BSF2 A 0 A_SetScale(0.4)
	  BSF2 B 2
	  BSF2 A 0 A_SetScale(0.3)
	  BSF2 C 2
	  BSF2 A 0 A_SetScale(0.2)
	  BSF2 D 2
	  Stop
   Death:
      BSF2 A 0
      Stop
   } 
}

ACTOR FireGuardFlamesFXSmall //Deals no damage.
{
   Radius 3
   Height 3
   Speed 0
   Scale 0.2
   RenderStyle Add
   Alpha 0.03
   +BRIGHT
   +THRUACTORS
   +DONTBLAST
   +NOGRAVITY
   +NOINTERACTION
   States
   {
   Spawn:
      BSF2 A 2
	  BSF2 A 0 A_SetScale(0.25)
	  BSF2 B 2
	  BSF2 A 0 A_SetScale(0.3)
	  BSF2 C 2
	  BSF2 A 0 A_SetScale(0.35)
	  BSF2 D 2
	  Stop
   Death:
      BSF2 A 1
      Stop
   } 
}

ACTOR FireGuardActivator : Inventory
{
   -INVBAR
   +UNDROPPABLE
   Inventory.MaxAmount 1
}

ACTOR BlazeStreamFireNovaFX
{
   Radius 10
   Height 5
   RenderStyle "Add"
   Alpha 0.55
   +BRIGHT
   +THRUACTORS
   +FLATSPRITE
   +ROLLSPRITE
   +DONTSPLASH
   +NOGRAVITY
   Scale 0.85
   ReactionTime 12
   States
   {
   Spawn:
      BSFV A 1 NoDelay A_SetPitch(pitch+frandom(-5,8)) //+10
	  TNT1 A 0 A_SetAngle(angle+frandom(0,359))
	  TNT1 A 0 A_SetRoll(roll+frandom(0,359))
	  BSFV ABC 2
   SpawnFall:
      BSFV C 2
	  BSFV A 0 A_SetScale(scalex+0.1)
	  TNT1 A 0 A_CountDown
      Loop
   Death:
      BSFV D 1
	  BSFV A 0 A_SetScale(scalex+0.05)
      BSFV A 0 A_FadeOut(0.2)
      Loop	  
   } 
}